# -*- coding: utf-8 -*-

import re

import requests
from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import VSlog

UA = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0'


class cHoster(iHoster):

    def __init__(self):
        iHoster.__init__(self, 'qiwi', 'Qiwi')

    def _getMediaLinkForGuest(self):
        VSlog(self._url)

        headers = {
            'User-Agent': UA,
            'Origin': self._url.rsplit('/', 1)[0],
            'Referer': self._url}

        s = requests.session()

        api_call = ''

        file_id = self._url.split("/")[-1]
        try:
            sHtmlContent = s.get(self._url, headers=headers).text
        except Exception as e:
            VSlog('Error: ' + str(e))
        aResult = re.search(r'class="page_TextHeading__VsM7r">(.+?)</h1>', sHtmlContent)
        if aResult:
            ext = aResult.group(1).split('.')[-1]
            VSlog(ext)
            api_call = f"https://qiwi.lol/{file_id}.{ext}"

        if api_call:
            return True, api_call + '|User-Agent=' + UA + '&Referer=' + self._url

        return False, False
