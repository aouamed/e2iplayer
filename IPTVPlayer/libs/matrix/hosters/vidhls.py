﻿# -*- coding: utf-8 -*-
#############################################################
# Yonn1981 https://github.com/Yonn1981/Repo
#############################################################


import requests
from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.requestHandler import \
    cRequestHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.parser import cParser

UA = 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Mobile Safari/537.36'


class cHoster(iHoster):

    def __init__(self):
        iHoster.__init__(self, 'vidhls', 'vidHLS')

    def setUrl(self, sUrl):
        self._url = str(sUrl).replace(".html", "")

    def _getMediaLinkForGuest(self):
        oParser = cParser()
        sUrl = self._url
        if '|Referer=' in sUrl:
            Referer = sUrl.split('|Referer=')[1]
            sUrl = sUrl.split('|Referer=')[0]
        else:
            sUrl = sUrl
            Referer = 'https://movtime8.store/'

        oRequest = cRequestHandler(self._url)
        sHtmlContent = oRequest.request()

        sdata = sUrl.split('data=')[1]

        Sgn = requests.Session()

        hdr = {'Sec-Fetch-Mode': 'navigate',
               'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
               'Accept-Language': 'en-US,en;q=0.9,ar;q=0.8',
               'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Mobile Safari/537.36 Edg/114.0.1823.82',
               'Upgrade-Insecure-Requests': '1',
               'Referer': Referer}
        prm = {"data": sdata}
        _r = Sgn.post(sUrl, headers=hdr, data=prm)
        sHtmlContent = _r.content.decode('utf8', errors='ignore').replace('\\', '')

        sPattern = '"videoServer":"([^"]+)'
        aResult = oParser.parse(sHtmlContent, sPattern)

        if aResult[0]:
            VidServ = aResult[1][0]

        sPattern = '"videoUrl":"([^"]+)'
        aResult = oParser.parse(sHtmlContent, sPattern)

        if aResult[0]:
            Url2 = aResult[1][0]
            Url2 = 'https://vidhls.com'+Url2+'?s='+VidServ
            s = requests.Session()
            headers = {'Referer': Referer}
            r = s.get(Url2, headers=headers)
            sHtmlContent = r.text
            sPattern = 'RESOLUTION=(\d+x\d+)\s*(https.*?=)'
            aResult = oParser.parse(sHtmlContent, sPattern)
            if aResult[0]:
                for aEntry in aResult[1]:
                    Url2 = aEntry[1]
                    api_call = Url2

        if api_call:
            return True, api_call

        return False, False
