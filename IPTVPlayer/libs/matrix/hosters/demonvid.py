﻿# -*- coding: utf-8 -*-
import re

from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import (VSlog,
                                                                    dialog)
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.requestHandler import \
    cRequestHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.packer import cPacker
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.parser import cParser


class cHoster(iHoster):

    def __init__(self):
        iHoster.__init__(self, 'demonvid', 'demonvid')

    def isDownloadable(self):
        return True

    def _getMediaLinkForGuest(self):
        VSlog(self._url)
        api_call = ''

        oRequest = cRequestHandler(self._url)
        sHtmlContent = oRequest.request()
        oParser = cParser()

        sPattern = 'file:"(https.+?)"'
        aResult = re.findall(sPattern, sHtmlContent)
        if aResult:
            api_call = aResult[0]  # fichier master valide pour la lecture
            oRequestHandler = cRequestHandler(api_call)
            sHtmlContent2 = oRequestHandler.request()
            list_url = []
            list_q = []
            oParser = cParser()
            sPattern = 'PROGRAM.*?BANDWIDTH.*?RESOLUTION=(\d+x\d+).*?(https.*?m3u8)'
            aResult = oParser.parse(sHtmlContent2, sPattern)
            if aResult[0]:
                for aEntry in aResult[1]:
                    list_url.append(aEntry[1])
                    list_q.append(aEntry[0])
                if list_url:
                    api_call = dialog().VSselectqual(list_q, list_url)

        sPattern = '(eval\(function\(p,a,c,k,e(?:.|\s)+?\))<\/script>'
        aResult = oParser.parse(sHtmlContent, sPattern)
        import unicodedata

        if aResult[0]:
            data = aResult[1][0]
            data = unicodedata.normalize('NFD', data).encode('ascii', 'ignore').decode('unicode_escape')
            sHtmlContent2 = cPacker().unpack(data)
            sHtmlContent2 = sHtmlContent2.replace('[', ';').replace(']', ';')
            list_url = []
            list_q = []

            sPattern = ';(.+?);(https.+?),'
            aResult = oParser.parse(sHtmlContent2, sPattern)
            if aResult[0]:
                for aEntry in aResult[1]:
                    list_url.append(aEntry[1])
                    list_q.append(aEntry[0])
                if list_url:
                    api_call = dialog().VSselectqual(list_q, list_url)

        if api_call:
            return True, api_call

        return False, False
