﻿from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import (VSlog,
                                                                    dialog)
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.handler.requestHandler import \
    cRequestHandler
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.parser import cParser

UA = 'Android'


class cHoster(iHoster):

    def __init__(self):
        iHoster.__init__(self, 'govidme', 'Govid')

    def isDownloadable(self):
        return True

    def _getMediaLinkForGuest(self):
        oParser = cParser()
        VSlog(self._url)

        oRequest = cRequestHandler(self._url)
        oRequest.addHeaderEntry('Referer', 'https://cima-club.io/')
        oRequest.addHeaderEntry('User-Agent', UA)
        sHtmlContent = oRequest.request()

        api_call = ''

        sPattern = 'file:"([^<]+)",label:"([^<]+)"}'
        aResult = oParser.parse(sHtmlContent, sPattern)
        if aResult[0]:
            # initialisation des tableaux
            url = []
            qua = []
            # Replissage des tableaux
            for i in aResult[1]:
                url.append(str(i[0]).replace("[", "%5B").replace("]", "%5D").replace("+", "%20"))
                qua.append(str(i[1]))

            api_call = dialog().VSselectqual(qua, url)

            if api_call:
                return True, api_call + '|User-Agent=' + UA+'&AUTH=TLS&verifypeer=false' + '&Referer=' + self._url
        else:
            return True, self._url

        return False, False
