# -*- coding: utf-8 -*-

import re

import requests
from Plugins.Extensions.IPTVPlayer.libs.matrix.hosters.hoster import iHoster
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.comaddon import (VSlog,
                                                                    dialog)
from Plugins.Extensions.IPTVPlayer.libs.matrix.lib.parser import cParser

UA = 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0'


class cHoster(iHoster):

    def __init__(self):
        iHoster.__init__(self, '2embedme', '-2embed.me')

    def _getMediaLinkForGuest(self):
        self._url = self._url.replace('/movie/', '/player/movie/')
        VSlog(self._url)
        referer = self._url
        if '?Referer=' in self._url:
            referer = self._url.split('?Referer=')[1]
            self._url = self._url.split('?Referer=')[0]

        headers = {
            'User-Agent': UA,
            'Referer': referer}

        s = requests.session()
        sHtmlContent = s.get(self._url, headers=headers).text

        api_call = ''
        SubTitle = ''

        oParser = cParser()
        sStart = 'tracks'
        sEnd = '</script>'
        sHtmlContent0 = oParser.abParse(sHtmlContent, sStart, sEnd).replace('\\', '')

        aResult = re.findall(r'"file":"(.*?)","label":"(.*?)"', sHtmlContent0)
        if aResult:
            url = []
            qua = []
            for i in aResult:
                url.append(str(i[0]))
                qua.append(str(i[1]))
            SubTitle = dialog().VSselectsub(qua, url)

        aResult = re.search(r'sources:\s*\[{"file":"(.*?)"', sHtmlContent)
        if aResult:
            api_call = aResult.group(1).replace('\\', '')

        if api_call:
            if SubTitle:
                return True, api_call + '|User-Agent=' + UA + '&Referer=' + referer, SubTitle
            else:
                return True, api_call + '|User-Agent=' + UA + '&Referer=' + referer

        return False, False
