# -*- coding: utf-8 -*-
# vStream https://github.com/Kodi-vStream/venom-xbmc-addons
import json
import time
import unicodedata
from datetime import date, datetime

from Plugins.Extensions.IPTVPlayer.libs.vstream.lib.comaddon import (addon,
                                                                     dialog,
                                                                     listitem)
from Plugins.Extensions.IPTVPlayer.libs.vstream.lib.tmdb import cTMDb
from Plugins.Extensions.IPTVPlayer.libs.xbmc import xbmc, xbmcgui, xbmcvfs
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import GetCacheSubDir


class GestionCookie():
    PathCache = GetCacheSubDir('Tsiplayer')[:-1]

    def MakeListwithCookies(self, c):
        t = {}
        c = c.split(';')
        for i in c:
            j = i.split('=', 1)
            if len(j) > 1:
                t[j[0]] = j[1]

        return t

    def DeleteCookie(self, Domain):
        Name = '/'.join([self.PathCache, 'cookie_%s.txt']) % Domain
        xbmcvfs.delete(Name)

    def SaveCookie(self, Domain, data):
        Name = '/'.join([self.PathCache, 'cookie_%s.txt']) % Domain

        f = xbmcvfs.File(Name, 'w')
        f.write(data)
        f.close()

    def Readcookie(self, Domain):
        Name = '/'.join([self.PathCache, 'cookie_%s.txt']) % Domain

        try:
            f = xbmcvfs.File(Name)
            data = f.read()
            f.close()
        except:
            return ''

        return data

    def AddCookies(self):
        cookies = self.Readcookie(self.__sHosterIdentifier)
        return 'Cookie=' + cookies

    def MixCookie(self, ancien_cookies, new_cookies):
        t1 = self.MakeListwithCookies(ancien_cookies)
        t2 = self.MakeListwithCookies(new_cookies)
        # Les nouveaux doivent ecraser les anciens
        for i in t2:
            t1[i] = t2[i]

        cookies = ''
        for c in t1:
            cookies = cookies + c + '=' + t1[c] + ';'
        cookies = cookies[:-1]
        return cookies

# -------------------------------
#     Configuration gestion
# -------------------------------


class cConfig:

    def isDharma(self):
        return self.__bIsDharma

    def getSettingCache(self):
        return False

    def getAddonPath(self):
        return False

    def getRootArt(self):
        return False

    def getFileFav(self):
        return False

    def getFileDB(self):
        return False

    def getFileCache(self):
        return False


def WindowsBoxes(sTitle, siteUrl, metaType, year, sSite, sFav, sCat):

    ADDON = addon()
    DIALOG = dialog()

    # Sinon on gere par vStream via la lib TMDB
    sType = str(metaType).replace('1', 'movie').replace('2', 'tvshow').replace('3', 'collection').replace('4', 'anime').replace('5', 'season').replace('6', 'episode')

    try:
        tmdb_id = xbmc.getInfoLabel('ListItem.Property(TmdbId)')
        season = xbmc.getInfoLabel('ListItem.Season')
        episode = xbmc.getInfoLabel('ListItem.Episode')
        if sType == 'episode' and not episode:
            sType = 'season'
        if sType == 'season' and not season:
            sType = 'tvshow'
        meta = cTMDb().get_meta(sType, sTitle, tmdb_id=tmdb_id, year=year, season=season, episode=episode)
    except:
        DIALOG.VSok("Veuillez vider le cache des métadonnées Paramètre - outils - 'vider le cache de vStream'")
        pass

    # si rien ne marche
    if not meta['imdb_id'] and not meta['tmdb_id'] and not meta['tvdb_id']:

        # fenetre d'erreur
        DIALOG.VSinfo(ADDON.VSlang(30204))
        return

    # convertion plot si necessaire
    try:
        if meta['plot']:
            meta['plot'] = str(meta['plot'].encode('latin-1'), 'utf-8')
    except Exception as e:
        pass

    # convertion de la date au format JJ/MM/AAAA
    if 'premiered' in meta and meta['premiered']:
        releaseDate = datetime(*(time.strptime(meta['premiered'], '%Y-%m-%d')[0:6]))
        meta['releaseDate'] = releaseDate.strftime('%d/%m/%Y')
    else:
        meta['releaseDate'] = '-'

    # convertion de la durée en secondes -> heure:minutes
    if 'duration' in meta and meta['duration']:
        duration = meta['duration'] // 60  # En minutes
        durationH = duration // 60  # Nombre d'heures
        meta['durationH'] = '{:02d}'.format(int(durationH))
        meta['durationM'] = '{:02d}'.format(int(duration - 60*durationH))
    else:
        meta['durationH'] = 0
        meta['durationM'] = 0

    # affichage du dialog perso
    class XMLDialog(xbmcgui.WindowXMLDialog):

        ADDON = addon()

        def __init__(self, *args, **kwargs):
            xbmcgui.WindowXMLDialog.__init__(self)
            self.meta = kwargs['meta']
            pass

        def onInit(self):
            # par default le resumer#
            color = ADDON.getSetting('deco_color')
            self.setProperty('color', color)
            self.poster = 'https://image.tmdb.org/t/p/%s' % self.ADDON.getSetting('poster_tmdb')
            self.none_poster = 'https://eu.ui-avatars.com/api/?background=000&size=512&name=%s&color=FFF&font-size=0.33'

            self.setFocusId(9000)

            if 'cast' in meta:
                listitems = []
                data = json.loads(meta['cast'])
                for i in data:
                    slabel = i['name']
                    slabel2 = i['character']
                    if i.get('thumbnail'):
                        sicon = str(i['thumbnail'])
                    else:
                        sicon = self.none_poster % slabel
                    sid = i['id']
                    listitem_ = listitem(label=slabel, label2=slabel2)
                    listitem_.setProperty('id', str(sid))
                    listitem_.setArt({'icon': sicon})
                    listitems.append(listitem_)
                self.getControl(50).addItems(listitems)

            if 'crew' in meta:
                listitems2 = []
                data = json.loads(meta['crew'])
                for i in data:
                    slabel = i['name']
                    slabel2 = i['job']
                    if i.get('profile_path'):
                        sicon = self.poster + str(i['profile_path'])
                    else:
                        sicon = self.none_poster % slabel
                    sid = i['id']
                    listitem_ = listitem(label=slabel, label2=slabel2)
                    listitem_.setProperty('id', str(sid))
                    listitem_.setArt({'icon': sicon})
                    listitems2.append(listitem_)
                self.getControl(5200).addItems(listitems2)

            listitems3 = []
            if 'guest_stars' in meta and meta['guest_stars']:  # dans certains épisodes
                # Decodage python 3
                data = eval(meta['guest_stars'])
                for guest in data:
                    slabel = guest['name']
                    slabel2 = guest['character']
                    if guest['profile_path']:
                        sicon = self.poster + str(guest['profile_path'])
                    else:
                        sicon = self.none_poster % slabel
                    sid = guest['id']
                    listitem_ = listitem(label=slabel, label2=slabel2)
                    listitem_.setProperty('id', str(sid))
                    listitem_.setArt({'icon': sicon})
                    listitems3.append(listitem_)
                self.getControl(50).addItems(listitems3)

            meta['title'] = sTitle

            for prop in meta:
                # Py3 unicode == str.
                try:
                    if isinstance(meta[prop], unicode):
                        self.setProperty(prop, meta[prop].encode('utf-8'))
                    else:
                        self.setProperty(prop, str(meta[prop]))
                except:
                    if isinstance(meta[prop], str):
                        self.setProperty(prop, meta[prop].encode('utf-8'))
                    else:
                        self.setProperty(prop, str(meta[prop]))

        def credit(self, meta='', control=''):
            listitems = []

            if not meta:
                meta = [{u'id': 0, u'title': u'Aucune information', u'poster_path': u'', u'vote_average': 0}]

            try:
                for i in meta:
                    try:
                        sTitle = unicodedata.normalize('NFKD', i['title']).encode('ascii', 'ignore')
                    except:
                        sTitle = 'Aucune information'

                    if i['poster_path']:
                        sThumbnail = self.poster+str(i['poster_path'])
                    else:
                        sThumbnail = self.none_poster % sTitle

                    listitem_ = listitem(label=sTitle)
                    try:
                        listitem_.setInfo('video', {'rating': i['vote_average'].encode('utf-8')})
                    except:
                        listitem_.setInfo('video', {'rating': str(i['vote_average'])})

                    listitem_.setArt({'icon': sThumbnail})
                    listitems.append(listitem_)
                self.getControl(control).addItems(listitems)

            except Exception as e:
                pass

        def onClick(self, controlId):
            if controlId == 11:
                sTitle = self.meta['title']
                year = self.meta['year']
                from Plugins.Extensions.IPTVPlayer.libs.vstream.lib.ba import \
                    cShowBA
                cBA = cShowBA()
                cBA.SetSearch(sTitle)
                cBA.SetYear(year)
                cBA.SetMetaType(metaType)
                cBA.SetTrailerUrl(self.getProperty('trailer'))
                cBA.SearchBA(True)
                return

            elif controlId == 30:
                self.close()
                return

            elif controlId == 50 or controlId == 5200:
                item = self.getControl(controlId).getSelectedItem()
                sid = item.getProperty('id')
                grab = cTMDb()
                sUrl = 'person/' + str(sid)

                try:
                    meta = grab.getUrl(sUrl, term="append_to_response=movie_credits,tv_credits")
                    meta_credits = meta['movie_credits']['cast']
                    self.credit(meta_credits, 5215)

                    try:
                        sTitle = unicodedata.normalize('NFKD', meta['name']).encode('ascii', 'ignore')
                    except:
                        sTitle = 'Aucune information'

                    if not meta['deathday']:
                        today = date.today()
                        try:
                            birthday = datetime(*(time.strptime(meta['birthday'], '%Y-%m-%d')[0:6]))
                            age = today.year - birthday.year - ((today.month, today.day) < (birthday.month, birthday.day))
                            age = '%s Ans' % age
                        except:
                            age = ''
                    else:
                        age = meta['deathday']

                    self.setProperty('Person_name', sTitle)
                    self.setProperty('Person_birthday', meta['birthday'])
                    self.setProperty('Person_place_of_birth', meta['place_of_birth'])
                    self.setProperty('Person_deathday', str(age))
                    self.setProperty('Person_biography', meta['biography'])
                    self.setFocusId(9000)

                except:
                    return
                self.setProperty('vstream_menu', 'Person')

            # click sur similaire
            elif controlId == 9:
                sid = self.getProperty('tmdb_id')

                grab = cTMDb()
                sUrl_simil = 'movie/%s/similar' % str(sid)
                sUrl_recom = 'movie/%s/recommendations' % str(sid)

                try:
                    meta = grab.getUrl(sUrl_simil)
                    meta = meta['results']
                    self.credit(meta, 5205)
                except:
                    pass

                try:
                    meta = grab.getUrl(sUrl_recom)
                    meta = meta['results']
                    self.credit(meta, 5210)
                except:
                    return

            # click sur marque-page
            elif controlId == 10:
                metaBM = {}
                metaBM['siteurl'] = siteUrl
                metaBM['title'] = self.meta['title']
                metaBM['site'] = sSite
                metaBM['fav'] = sFav
                metaBM['cat'] = sCat
                metaBM['icon'] = self.meta['poster_path']
                metaBM['fanart'] = self.meta['backdrop_path']
                try:
                    from Plugins.Extensions.IPTVPlayer.libs.vstream.lib.db import \
                        cDb
                    with cDb() as db:
                        db.insert_bookmark(metaBM)
                except:
                    pass

            # click pour recherche
            elif controlId == 5215 or controlId == 5205 or controlId == 5210:

                import sys

                from Plugins.Extensions.IPTVPlayer.libs.vstream.lib.util import \
                    cUtil
                item = self.getControl(controlId).getSelectedItem()
                sTitle = cUtil().CleanName(item.getLabel())

                self.close()

                # Si lancé depuis la page Home de Kodi, il faut d'abord en sortir pour lancer la recherche
                if xbmc.getCondVisibility('Window.IsVisible(home)'):
                    xbmc.executebuiltin('ActivateWindow(%d)' % 10028)

                sTest = '%s?site=globalSearch&searchtext=%s&sCat=1' % (sys.argv[0], sTitle)
                xbmc.executebuiltin('Container.Update(%s)' % sTest)
                return

        def onFocus(self, controlId):
            self.controlId = controlId
            # fullscreen end return focus menu
            if controlId == 40:
                while xbmc.Player().isPlaying():
                    xbmc.sleep(500)
                    if not xbmc.Player().isPlaying():
                        self.setFocusId(9000)

        def _close_dialog(self):
            self.close()

        def onAction(self, action):
            if action.getId() in (104, 105, 1, 2):
                return

            if action.getId() in (9, 10, 11, 30, 92, 216, 247, 257, 275, 61467, 61448):
                self.close()

    path = 'special://home/addons/plugin.video.vstream'
    args = ('DialogInfo4.xml', path, 'default', '720p')
    kwargs = {}
    kwargs['meta'] = meta

    wd = XMLDialog(*args, **kwargs)

    wd.doModal()
    del wd
