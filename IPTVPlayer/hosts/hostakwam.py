# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.components.recaptcha_v2helper import \
    CaptchaHelper
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.p2p3.UrlLib import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta


def GetConfigList():
    optionList = []
    return optionList

def gettytul():
    return 'Akwam'


class Akwam(CBaseHostClass, CaptchaHelper):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'akwam', 'cookie': 'akwam.cookie'})

        self.MAIN_URL = 'https://ak.sv/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/0sFVdXH/akwam.png'

        self.HTTP_HEADER = {'User-Agent': self.defaultUserAgent, 'DNT': '1', 'Accept': 'text/html', 'Accept-Encoding': 'gzip, deflate', 'Referer': self.getMainUrl(), 'Origin': self.getMainUrl()}
        self.AJAX_HEADER = dict(self.HTTP_HEADER)
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest', 'Accept-Encoding': 'gzip, deflate', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8', 'Accept': 'application/json, text/javascript, */*; q=0.01'})
        self.defaultParams = {'header': self.HTTP_HEADER, 'with_metadata': True, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("Akwam.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'movei', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'serie', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'tvshow', 'title': _('بــرامــج'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG("Akwam.listCatItems cItem[%s]" % (cItem))
        category = self.currItem.get("category", '')

        if category == 'movei':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('أفلام أجنبية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?section=30')},
                {'category': 'list_items', 'title': _('أفلام عربية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?section=29')},
                {'category': 'list_items', 'title': _('أفلام أسيوية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?section=33')},
                {'category': 'list_items', 'title': _('أفلام تركية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?section=32')},
                {'category': 'list_items', 'title': _('أفلام هندية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?section=31')},
                {'category': 'list_items', 'title': _('أفلام مدبلجة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?section=0&category=71&rating=0&year=0&language=0&formats=0&quality=0')},
                {'category': 'list_items', 'title': _('أفلام كرتون'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?category=30')},
                {'category': 'list_items', 'title': _('أفلام وثائقية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/movies?category=28')}]
        elif category == 'serie':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('رمــضـــان'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?section=0&category=87&rating=0&year=0&language=0&formats=0&quality=0')},
                {'category': 'list_items', 'title': _('مسلسلات أجنبية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?section=30')},
                {'category': 'list_items', 'title': _('مسلسلات عربية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?section=29')},
                {'category': 'list_items', 'title': _('مسلسلات أسيوية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?section=33')},
                {'category': 'list_items', 'title': _('مسلسلات تركية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?section=32')},
                {'category': 'list_items', 'title': _('مسلسلات هندية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?section=31')},
                {'category': 'list_items', 'title': _('مسلسلات انمي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?category=30')},
                {'category': 'list_items', 'title': _('مسلسلات مدبلجة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?section=30&category=71&rating=0&year=0&language=0&formats=0&quality=0')},
                {'category': 'list_items', 'title': _('مسلسلات تركية مدبلجة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?section=32&category=71&rating=0&year=0&language=0&formats=0&quality=0')},
                {'category': 'list_items', 'title': _('مسلسلات هندية مدبلجة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?section=31&category=71&rating=0&year=0&language=0&formats=0&quality=0')},
                {'category': 'list_items', 'title': _('مسلسلات وثائقية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/shows?section=46&category=0&rating=0&year=0&formats=0&quality=0')},]
        elif category == 'tvshow':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('رمضان'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series?section=0&category=87&rating=0&year=0&language=0&formats=0&quality=0')},
                {'category': 'list_items', 'title': _('برامج تلفزيونية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/shows?section=42')},
                {'category': 'list_items', 'title': _('مصارعة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/shows?section=43&category=0&rating=0&year=0&formats=0&quality=0')},
                {'category': 'list_items', 'title': _('مسرحيات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/shows?section=45')}]
        self.listsTab(NEW_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG("Akwam.listItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'pagination'), ('</ul', '>'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(ph.decodeHtml(nextPage), '''href=['"]([^'^"]+?)['"][^>]*?>%s<''' % (page + 1))[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'widget'), ('<div', '>', 'd-none d-sm-block'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'col-lg-auto'), ('badge-secondary ml-1', '</span', '>'))
        for item in tmp:

            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''data-src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('text-white', '>'), ('</a', '>'), False)[1])
            desc = ph.extract_desc(item, [('year', '''badge-secondary ml-1['"].+?([0-9]{4})[$<]'''), ('quality', '''quality['"].+?([^>]+?)[$<]'''), ('rating', '''icon-star mr-2['"].+?([^>]+?)[$<]''')])

            info = ph.std_title(title, desc=desc, with_ep=True)
            if title != '':
                title = info.get('title_display')
            desc = info.get('desc')

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG("Akwam.exploreItems cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        # trailer
        trailerUrl = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'col-lg-2 col-md-3 col-sm-4 d-flex flex-column px-4 px-sm-0'), ('btn btn-light btn-pill d-flex align-items-center', '<span'))[1]
        trailerUrl = self.cm.ph.getSearchGroups(trailerUrl, '''[<]a href=['"]([^"^']+?)['"]''')[0]
        if self.cm.isValidUrl(trailerUrl):
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': '[\c00???020Trailer\c00??????]', 'url': trailerUrl, 'desc': ''})
            self.addVideo(params)

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('line-height', '<p>'), ('</p', '>'), False)[1])

        sHtmlContent = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'series-episodes'), ('<div', '>', 'widget-4'), True)[1]
        Season = self.cm.ph.getDataBeetwenMarkers(sHtmlContent, '>مواسم', ('</div', '>'), True)[1]
        if Season:
            self.addMarker({'title': '{}مـــواســم'.format('\c0000??00'), 'icon': cItem['icon'], 'desc': ''})
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(Season, '<a', ('</a', '>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('text-white', '>'), ('</a', '>'), False)[1])

                info = ph.std_title(title, with_ep=True)
                if title != '':
                    title = info.get('title_display')
                otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                self.addDir(params)

        Episod = self.cm.ph.getDataBeetwenMarkers(sHtmlContent, ('>الحلقات', '</a>'), ('<div', '>', 'widget-4'), True)[1]
        if Episod:
            self.addMarker({'title': '{}الـحـلـقـات'.format('\c0000??00'), 'icon': cItem['icon'], 'desc': ''})
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(Episod, ('<h2', '>'), ('</h2', '>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('text-white', '>'), ('</a', '>'), False)[1])

                info = ph.std_title(title, with_ep=True)
                if title != '':
                    title = info.get('title_display')
                otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                self.addVideo(params)
        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("Akwam.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        url = self.getFullUrl('/search?q={}'.format(urllib_quote_plus(searchPattern)))
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG("Akwam.getLinksForVideo [%s]" % cItem)
        urlTab = []

        if 'Trailer' in cItem['title']:
            return self.up.getVideoLinkExt(cItem['url'])

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'col-lg-6 col'), '>مشاهدة<', True)[1]
        siteUrl = self.getFullUrl(self.cm.ph.getSearchGroups(tmp, '''href=['"]([^"^']+?)['"]''')[0])

        sts, data = self.getPage(siteUrl)
        if not sts:
            return
        tmpUrl = self.getFullUrl(self.cm.ph.getSearchGroups(data, '''href=['"](http[^<]+/watch/.+?)['"]''')[0])

        sts, sHtmlContent = self.getPage(tmpUrl)
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(sHtmlContent, ('<div', '>', 'pt-3'), ('</video', '>'), True)[1]
        if tmp:
            urlTab = self.getLinKS(tmp, cItem['title'])
        else:
            tmp = self.cm.ph.getDataBeetwenMarkers(sHtmlContent, ('<div', '>', 'g-recaptcha'), ('</div', '>'), True)[1]
            sitekey = self.cleanHtmlStr(self.cm.ph.getSearchGroups(tmp, '''sitekey=['"]([^"^']+?)['"]''')[0])

            post_data = {}
            if sitekey != '':
                token, errorMsgTab = self.processCaptcha(sitekey, self.cm.meta['url'], captchaType="cf_re")
                if token != '':
                    vUrl = self.getFullUrl('verify')
                    post_data['g-recaptcha-response'] = token
                    params = dict(self.defaultParams)
                    params['handle_recaptcha'] = True
                    params['header']['Referer'] = tmpUrl

                    sts, data = self.getPage(vUrl, params, post_data)
                    if not sts:
                        return

                    tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'pt-3'), ('</video', '>'), True)[1]
                    urlTab = self.getLinKS(tmp, cItem['title'])

        return urlTab

    def getLinKS(self, data, TitLE):
        urlTab = []
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(data, '<source', '/>')
        for item in tmp:
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''size=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''', ignoreCase=True)[0])

            if title != '':
                title = ('{} \c00??7950 [{}]\c00??????\c00???020 - {}\c00??????'.format(TitLE, '%sP' % title, self.up.getHostName(url, True)))

            urlTab.append({'name': title, 'url': strwithmeta(url, {'Referer': self.getMainUrl()}), 'need_resolve': 0})

        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("Akwam.getVideoLinks [%s]" % videoUrl)

        if self.cm.isValidUrl(videoUrl):
            return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG("Akwam.getArticleContent [%s]" % cItem)
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<h1', '>', 'text-white mb-0'), ('<div', '>', 'd-md-none'), True)[1]

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('mx-2', '/'), ('</span', '>'), False)[1])
        if Info != '':
            otherInfo['imdb_rating'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('اللغة', ':'), ('</span', '>'), False)[1])
        if Info != '':
            otherInfo['language'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('الترجمة', ':'), ('</span', '>'), False)[1])
        if Info != '':
            otherInfo['translation'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('جودة', ':'), ('</span', '>'), False)[1])
        if Info != '':
            otherInfo['quality'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('انتاج', ':'), ('</span', '>'), False)[1])
        if Info != '':
            otherInfo['country'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('السنة', ':'), ('</span', '>'), False)[1])
        if Info != '':
            otherInfo['year'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('مدة', ':'), ('</span', '>'), False)[1])
        if Info != '':
            otherInfo['duration'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('badge-light', '>'), ('</div', '>'), False)[1])
        if Info != '':
            otherInfo['genre'] = Info
        return [{'title': cItem['title'], 'text': cItem['desc'], 'images': [{'title': '', 'url': self.getFullUrl(cItem['icon'])}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

    # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'movei' or category == 'serie' or category == 'tvshow':
            self.listCatItems(self.currItem)
        elif category == 'list_items':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, Akwam(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
