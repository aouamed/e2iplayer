# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.p2p3.UrlLib import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta


def GetConfigList():
    optionList = []
    return optionList

def gettytul():
    return 'FajerShow'


class FajerShow(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'alfajertv', 'cookie': 'alfajertv.cookie'})

        self.MAIN_URL = 'https://show.alfajertv.com/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/Vmc6jXz/alfajertv.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("FajerShow.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'movei', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'serie', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'other', 'title': _('رمـــضــان'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'msrh', 'title': _('مسـرحـيـات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/genre/plays/')},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG("FajerShow.listCatItems cItem[%s]" % (cItem))
        category = self.currItem.get("category", '')

        sts, data = self.getPage(self.getMainUrl())
        if not sts:
            return

        if category == 'other':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('رمـضـان 2023'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/genre/ramadan2023/')},
                {'category': 'list_items', 'title': _('رمـضـان 2024'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/genre/ramadan2024')},]
            self.listsTab(NEW_CAT_TAB, cItem)
        else:
            if category == 'movei':
                sStart = '>أفلام<'
            elif category == 'serie':
                sStart = '>مسلسلات<'

            tmp = self.cm.ph.getDataBeetwenMarkers(data, sStart, ('</ul', '>'), True)[1]
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', 'menu-item'), ('</li', '>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('</a', '>'), False)[1])

                params = dict(cItem)
                params.update({'category': 'list_items', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': ''})
                self.addDir(params)

    def listItems(self, cItem):
        printDBG("FajerShow.listItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'pagination'), ('</div', '>'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''href=['"]([^'^"]+?)['"][^>]*?>%s<''' % (page + 1))[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'content'), ('<div', '>', 'sidebar scrolling'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<article', '>'), ('</article', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<h3', '>'), ('</h3', '>'), False)[1])
            if title == '':
                title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''alt=['"]([^"^']+?)['"]''')[0])
            desc = ph.extract_desc(item, [('year', '''metadata.+?<span>([0-9]{4})</span>'''), ('quality', '''quality['"].+?([^>]+?)[$<]'''), ('rating', '''icon-star2['"].+?([^>]+?)[$<]''')])

            info = ph.std_title(title, desc=desc, with_ep=True)
            if title != '':
                title = info.get('title_display')
            desc = info.get('desc')

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG("FajerShow.exploreItems cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        # trailer
        trailerUrl = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'trailer'), ('</iframe', '>'))[1]
        trailerUrl = self.cm.ph.getSearchGroups(trailerUrl, '''src=['"]([^"^']+?)['"]''')[0]
        if self.cm.isValidUrl(trailerUrl):
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': '[\c00???020Trailer\c00??????]', 'url': trailerUrl, 'desc': ''})
            self.addVideo(params)

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('<div', '>', 'wp-content'), ('</p', '>'), False)[1])

        Season = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'seasons'), ('<div', '>', 'cast'), True)[1]
        if Season:
            tmpSeason = self.cm.ph.getAllItemsBeetwenMarkers(Season, ('<div', '>', 'se-q'), ('</ul', '>'))
            for itemS in tmpSeason:
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(itemS, ('title', '>'), ('<i', '>'), False)[1])
                self.addMarker({'title': '\c0000??00%s' % title, 'icon': cItem['icon'], 'desc': ''})
                tmp = self.cm.ph.getAllItemsBeetwenMarkers(itemS, ('<div', '>', 'episodiotitle'), ('</li', '>'))
                for item in tmp:
                    url = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                    title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('</a', '>'), False)[1])

                    info = ph.std_title(title, with_ep=True)
                    if title != '':
                        title = info.get('title_display')
                    otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                    params = dict(cItem)
                    params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                    self.addVideo(params)
        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("FajerShow.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        url = self.getFullUrl('/?s={}'.format(urllib_quote_plus(searchPattern)))
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG("FajerShow.getLinksForVideo [%s]" % cItem)
        urlTab = []

        if 'Trailer' in cItem['title']:
            return self.up.getVideoLinkExt(cItem['url'])

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'playeroptionsul'), ('</ul', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', 'dooplay_player_option'), ('</li', '>'))
        for item in tmp:
            sPost = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-post=['"]([^"^']+?)['"]''')[0])
            sType = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-type=['"]([^"^']+?)['"]''')[0])
            sNume = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-nume=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title['"].+?([^>]+?)[$<]''')[0])

            siteUrl = self.getFullUrl('/wp-admin/admin-ajax.php')
            params = {'action': 'doo_player_ajax', 'post': sPost, 'nume': sNume, 'type': sType}
            sts, data = self.getPage(siteUrl, post_data=params)
            if not sts:
                return

            url = self.getFullUrl(self.cm.ph.getSearchGroups(data, '''src=['"]([^"^']+?)['"]''', ignoreCase=True)[0])

            if title != '':
                title = ('{} \c00??7950 [{}]\c00??????\c00???020 - {}\c00??????'.format(cItem['title'], title, self.up.getHostName(url, True)))

            urlTab.append({'name': title, 'url': strwithmeta(url, {'Referer': self.getMainUrl()}), 'need_resolve': 1})

        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("FajerShow.getVideoLinks [%s]" % videoUrl)

        if self.cm.isValidUrl(videoUrl):
            return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG("FajerShow.getArticleContent [%s]" % cItem)
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'info'), ('<div', '>', 'sbox'), True)[1]

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('repimdb', '>'), ('</strong', '>'), False)[1])
        if Info != '':
            otherInfo['imdb_rating'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('تاريخ', '>'), ('</span', '>'), False)[1])
        if Info != '':
            otherInfo['year'] = Info

        return [{'title': cItem['title'], 'text': cItem['desc'], 'images': [{'title': '', 'url': self.getFullUrl(cItem['icon'])}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

    # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'movei' or category == 'serie' or category == 'other':
            self.listCatItems(self.currItem)
        elif category == 'list_items' or category == 'msrh':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, FajerShow(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
