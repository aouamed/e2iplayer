# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS

from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc


def GetConfigList():
    optionList = []
    return optionList

def gettytul():
    return 'Aljazeera'


class Aljazeera(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'cookie': 'aljazeera.cookie'})

        self.MAIN_URL = 'https://www.aljazeera.net/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/fHdY2S8/aljazeera.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("Aljazeera.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'show_movies', 'title': _('افلام وثائقية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/programs/investigative/')},
            {'category': 'show_series', 'title': _('مسلسلات وثائقية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/programs/documentaries/')},
            {'category': 'show_program', 'title': _('برامج تلفزيونية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/programs/newsmagazineshows/')}]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG("Aljazeera.listItems cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'container--section-programs'), ('<div', '>', 'container--footer container--black'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'responsive-image'), ('</article', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+\.jpe?g)''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<span', '>'), ('</span', '>'), False)[1])
            desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<p', '>', 'program-card__description'), ('</p', '>'), False)[1])

            info = ph.std_title(title, with_ep=True)
            if title != '':
                title = info.get('title_display')

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG("Aljazeera.exploreItems cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', 'featured-news-container'), ('<section', 'news-feed-container'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'responsive-image'), ('</article', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+\.jpe?g)''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])
            if title == '':
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<h3', '>', 'article-card__title'), ('</a', '>'), False)[1])
            desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<div', '>', 'article-card__excerpt'), ('</p', '>'), False)[1])

            info = ph.std_title(title, with_ep=True)
            if title != '':
                title = info.get('title_display')

            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
            self.addVideo(params)

    def getLinksForVideo(self, cItem):
        printDBG("Aljazeera.getLinksForVideo [%s]" % cItem)
        urlTab = []

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        url = self.getFullUrl(self.cm.ph.getSearchGroups(data, '''embedUrl['"]:['"]([^"^']+?)['"]''')[0])
        urlTab.append({'name': '', 'url': url, 'need_resolve': 1})
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("Aljazeera.getVideoLinks [%s]" % videoUrl)

        return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG("Aljazeera.getArticleContent [%s]" % cItem)
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, 'article-header">', 'article-content-read-more', True)[1]

        title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('u-clickable-card__link', '>', '<span>'), ('</span>', '</a>'), False)[1])
        if title == '':
            title = cItem['title']

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('article-excerpt', '>'), ('</div>', '<button'), False)[1])
        if desc == '':
            desc = cItem['desc']

        return [{'title': title, 'text': desc, 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

        # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'show_movies':
            self.listItems(self.currItem)
        elif category == 'show_series':
            self.listItems(self.currItem)
        elif category == 'show_program':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, Aljazeera(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
