# coding: utf-8

import datetime
import glob
import os
import re
import sys
import time

from Plugins.Extensions.IPTVPlayer.components.e2ivkselector import \
    GetVirtualKeyboard
from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    GetIPTVSleep
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs.e2ijson import loads as json_loads
from Plugins.Extensions.IPTVPlayer.libs.urlparser import urlparser
from Plugins.Extensions.IPTVPlayer.libs.urlparserhelper import \
    getDirectM3U8Playlist
from Plugins.Extensions.IPTVPlayer.libs.vstream.lib.comaddon import addon
from Plugins.Extensions.IPTVPlayer.libs.vstream.lib.gui.hoster import \
    cHosterGui
from Plugins.Extensions.IPTVPlayer.libs.vstream.lib.home import cHome
from Plugins.Extensions.IPTVPlayer.libs.vstream.lib.tmdb import cTMDb
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import (GetCacheSubDir,
                                                           printDBG, printExc)
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta


def GetConfigList():
    optionList = []
    return optionList

def gettytul():
    return 'vStream'


class vStream(CBaseHostClass):
    def __init__(self):
        CBaseHostClass.__init__(self, {'cookie': 'vstream.cookie'})

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

        self.SiteFIle = '/usr/lib/enigma2/python/Plugins/Extensions/IPTVPlayer/libs/vstream/sites'
        self.fncs_search = ['showsearch', 'myshowsearchmovie', 'myshowsearchserie', 'showmoviessearch', 'showsearchtext']
        self.MAIN_IMP = 'from {}'.format(self.SiteFIle.replace('/usr/lib/enigma2/python/', '').replace('/', '.'))
        self.MyPath = GetCacheSubDir('Tsiplayer')
        self.workflag = '{}addon_working.txt'.format(self.MyPath)

    @staticmethod
    def timeTostr(time_):
        return str(datetime.timedelta(seconds=time_))

    @staticmethod
    def get_url_meta(URL):
        printDBG('get_url_meta='+URL)
        tags = ''
        meta_ = {}
        if '|' in URL:
            URL, tags = URL.split('|')
            if tags != '':
                if '&' in tags:
                    tags = tags.split('&')
                    for tag in tags:
                        id_, val_ = tag.split('=', 1)
                        meta_[id_] = val_.replace('+', ' ')
                else:
                    id_, val_ = tags.split('=')
                    meta_[id_] = val_
        return (URL, meta_)

    @staticmethod
    def replaceColors(titre):
        titre = str(titre)
        color_replace = [('%5BCOLOR+coral%5D', '\c00??7950'), ('%5B%2FCOLOR%5D', '\c00??????'), ('%5BCOLOR+COLOR+gold%5D', '\c00??9900'),
                         ('%5BCOLOR+COLOR+violet%5D', '\c00??0099'), ('%5BCOLOR+COLOR+orange%5D', '\c00??6600'), ('%5BCOLOR+COLOR+dodgerblue%5D', '\c00??90??')]

        color_replace = [('[COLOR violet]', '\c00??90??'), ('[COLOR dodgerblue]', '\c007070??'), ('[COLOR lightcoral]', '\c00?08080'), ('[/COLOR]', '\c00??????'),
                         ('[COLOR gold]', '\c00????00'), ('[COLOR orange]', '\c00???020'), ('[COLOR red]', '\c00??5555'), ('[COLOR skyblue]', '\c0000????'),
                         ('[COLOR teal]', '\c00009999'), ('[COLOR coral]', '\c00??7950'), ('[COLOR khaki]', '\c00997050'), ('[COLOR 0]', '\c00??????'),
                         ('[COLOR crimson]', '\c00??5555'), ('[COLOR grey]', '\c00999999'), ('[COLOR olive]', '\c00808000'), ('[COLOR fuchsia]', '\c00??40??'),
                         ('[COLOR yellow]', '\c00????33'), ('[COLOR aqua]', '\c000030??'), ('[COLOR cyan]', '\c0030????'),]

        for cl0, cl1 in color_replace:
            titre = titre.replace(cl0, cl1)
            titre = titre.replace(cl0, cl1)
        return titre

    @staticmethod
    def getDesc(Items):
        desc0 = ''
        genre = str(Items.get('genre', ''))
        sDescription = Items.get('plot', '')
        year = str(Items.get('year', ''))
        rating = Items.get('rating', 0)
        duration = Items.get('duration', 0)
        genre = str(Items.get('genre', ''))
        sDescription = vStream.replaceColors(sDescription)
        try:
            rating = "{:.1f}".format(rating)
        except:
            rating = str(rating)
        try:
            duration = vStream.timeTostr(duration)
        except:
            duration = str(duration)

        if rating.strip() == '0.0':
            rating = ''
        if year.strip() == '0':
            year = ''
        if duration.strip() == '0:00:00':
            duration = ''
        if rating == '0':
            rating = ''
        if duration == '0':
            duration = ''
        if genre == '0':
            genre = ''
        if sDescription == '0':
            sDescription = ''
        if (rating != '') and (rating != '0'):
            desc0 = '{}{}TMDB: {}{}|'.format(desc0, '\c00????00', '\c00??????', rating)
        if year != '':
            desc0 = '{}{}Year: {}{}|'.format(desc0, '\c00????00', '\c00??????', year)
        if duration != '':
            desc0 = '{}{}Duration: {}{}|'.format(desc0, '\c00????00', '\c00??????', duration)
        if genre != '':
            desc0 = '{}{}Genre: {}{}|'.format(desc0, '\c00????00', '\c00??????', genre)
        if desc0.strip() != '':
            desc0 = '{}\n{}'.format(desc0, sDescription)
        else:
            desc0 = sDescription
        return desc0

    def listMainMenu(self, cItem):
        printDBG("vStream.listMainMenu cItem[%s]" % (cItem))

        sys.argv = ''
        oHome = cHome()
        oHome.load()
        self.exploreItems(cItem)

    def listItems(self, cItem):
        printDBG("vStream.listItems cItem[%s]" % (cItem))

        sFunction = cItem.get('sFunction', 'load')
        sSiteName = cItem.get('sSiteName', '')
        sys.argv = cItem.get('argv', ['plugin://plugin.video.vstream/', '13', '?'])
        import_ = '{}.{} import '.format(self.MAIN_IMP, sSiteName)
        if sFunction.lower() in self.fncs_search:
            if sSiteName != 'globalSearch':
                self.write_search()

        if (sSiteName == 'globalSearch'):
            sFunction = 'showSearch'
            f = open(self.workflag, "w")
            f.write('OK')
            f.close()

        if (sSiteName == 'cHome'):
            oHome = cHome()
            printDBG('exec=oHome.{}()'.format(sFunction))
            exec('oHome.{}()'.format(sFunction))
        else:
            exec(import_+sFunction)
            exec('{}()'.format(sFunction))

        if os.path.exists(self.workflag):
            os.remove(self.workflag)

        self.exploreItems(cItem)

    def exploreItems(self, cItem):
        printDBG("vStream.exploreItems cItem[%s]" % (cItem))

        addons = addon()
        path_listing = '{}TsiPlayer_listing.json'.format(self.MyPath)

        with open(path_listing) as f:
            listing = f.read()
        listing = json_loads(listing)
        nb_list = len(listing)
        for Items in listing:
            icon = str(Items.get('icon', ''))
            thumb = str(Items.get('thumb', ''))
            if thumb != '':
                icon = thumb

            icon = icon.replace('plugin.video.xxxx', 'plugin.video.vstream')
            icon = icon.replace('special://home/addons/plugin.video.vstream/resources/', 'file:///usr/lib/enigma2/python/Plugins/Extensions/IPTVPlayer/libs/vstream/')
            sTitle = Items.get('title', '')
            sTitle = vStream.replaceColors(sTitle)
            sDescription = vStream.getDesc(Items)
            if sDescription == '':
                sDescription = cItem.get('desc', '')
            sSiteUrl = Items.get('siteUrl', '')
            sSiteName = Items.get('sId', '')
            sFunction = Items.get('sFav', '')
            sFileName = Items.get('sFileName', '')
            sParams = Items.get('sParams', '')
            sMeta = Items.get('sMeta', '')
            argv = ['plugin://plugin.video.vstream/', '13', '?'+sParams]
            Year = ''
            sHosterIdentifier = Items.get('sHosterIdentifier', '')
            sMediaUrl = Items.get('sMediaUrl', '')

            sMeta = str(sMeta).replace('1', 'movie').replace('2', 'tvshow').replace('3', 'collection').replace('4', 'anime').replace('7', 'person').replace('8', 'network')

            if sMeta in ['tvshow', 'movie', 'collection', 'anime', 'person', 'network']:
                EPG = True
            else:
                EPG = False
            if '&' in sParams:
                _item__ = sParams.split('&')
            else:
                _item__ = [sParams]
            for _item_ in _item__:
                if '=' in _item_:
                    key_, val_ = _item_.split('=', 1)
                    if key_ == 'sThumb':
                        icon = val_.replace('%2F', '/').replace('%3A', ':').replace('%3F', '?').replace('%3D', '=').replace('%3C', ',').replace('%7C', '|')

            if (addons.VSlang(30033) != sTitle.strip()) and (addons.VSlang(30455) != sTitle.strip()) and (addons.VSlang(30125) != sTitle.strip()) and (addons.VSlang(30308) != sTitle.strip()) and (addons.VSlang(30207) != sTitle.strip()):
                if sFunction == 'DoNothing':
                    if (nb_list == 1) and (sTitle.strip() == ''):
                        sTitle = '{}No informations'.format('\c00??8888')
                    self.addMarker({'title': sTitle, 'desc': '', 'icon': icon})
                elif ((sFunction == 'play') or ((sSiteName == 'radio') and (sFunction == ''))) or (sHosterIdentifier == 'lien_direct'):
                    if (sMediaUrl != ''):
                        url = sMediaUrl
                    else:
                        url = sSiteUrl

                    if (sHosterIdentifier == 'lien_direct'):
                        host = 'direct'
                    else:
                        host = 'none'
                    host = 'tshost'
                    color = ''
                    host_ = urlparser.getDomain(url).replace('www.', '')
                    if sHosterIdentifier == 'lien_direct':
                        color = '\c0060??60'
                    elif urlparser().checkHostSupportbyname(host_):
                        color = '\c0090??20'
                    elif urlparser().checkHostNotSupportbyname(host_):
                        color = '\c00??3030'
                    elif urlparser().checkHostSupportbyname_e2iplayer(host_):
                        color = '\c00????60'
                    regexp = re.compile(r'[ء-ي]')
                    if (regexp.search(sTitle)) and (not sTitle.startswith('I-')):
                        sTitle = 'I- {}'.format(sTitle)
                    sTitle = '| {} | {}{}'.format(sTitle, color, urlparser.getDomain(url).replace('www.', '').title())
                    sDescription = '{}Host: {}{}\n{}'.format('\c00????00', '\c00??????', sHosterIdentifier.title(), sDescription)
                    Params = {'EPG': EPG, 'sMeta': sMeta, 'good_for_fav': True, 'category': 'video', 'url': url, 'sHosterIdentifier': sHosterIdentifier, 'title': sTitle, 'desc': sDescription, 'icon': icon, 'hst': host, 'gnr': 1}
                    self.addVideo(Params)
                elif sTitle != 'None':
                    Params = {
                        'good_for_fav': True, 'EPG': EPG, 'sMeta': sMeta, 'sFileName': sFileName, 'Year': Year, 'category': 'list_items', 'title': sTitle, 'sFunction': sFunction, 'sSiteUrl': sSiteUrl, 'desc': sDescription, 'sSiteName': sSiteName, 'argv': argv, 'icon': icon}
                    self.addDir(Params)

    def getLinksForVideo(self, cItem):
        printDBG("vStream.getLinksForVideo [%s]" % cItem)
        urlTab = []
        gnr = cItem.get('gnr', 0)
        sHosterIdentifier = cItem['sHosterIdentifier']
        sMediaUrl = cItem['url']

        if (sHosterIdentifier == 'lien_direct') or (sHosterIdentifier == ''):
            gnr = 0
        if gnr == 0:
            URL, meta_ = vStream.get_url_meta(sMediaUrl)
            if 'm3u8' in URL:
                URL = strwithmeta(URL, meta_)
                urlTab = getDirectM3U8Playlist(URL, False, checkContent=True, sortWithMaxBitrate=999999999)
            else:
                URL = strwithmeta(URL, meta_)
                urlTab.append({'name': 'Direct', 'url': URL, 'need_resolve': 0})
        elif gnr == 1:
            TryMatrix = False
            try:
                cHoster = cHosterGui()
                oHoster = cHoster.getHoster(sHosterIdentifier)
                oHoster.setUrl(sMediaUrl)
                aLink = oHoster.getMediaLink()
                printDBG('aLink='+str(aLink))
            except Exception as e:
                aLink = [False, '']
                printExc()
            if aLink:
                if (aLink[0] == True):
                    URL = aLink[1]
                    if '||' in URL:
                        urls = URL.split('||')
                    else:
                        urls = [URL]
                    for URL in urls:
                        if URL.strip() != '':
                            label = ''
                            if '|tag:' in URL:
                                URL, label = URL.split('|tag:', 1)
                            printDBG('URL='+URL)
                            if URL.startswith('//'):
                                URL = 'http:'+URL
                            URL, meta = vStream.get_url_meta(URL)
                            URL = strwithmeta(URL, meta)

                            printDBG('URL='+URL)
                            urlTab.append({'url': URL, 'name': sHosterIdentifier+' '+label})
                else:
                    TryMatrix = True
            else:
                TryMatrix = True
            if TryMatrix:
                printDBG('Try with UrlParser Parser')
                if (urlparser().checkHostSupport(str(sMediaUrl)) == 1):
                    url_ = str(sMediaUrl).replace('://www.youtube.com/embed/', '://www.youtube.com/watch?v=')
                    printDBG('UrlParser Parser Found :{}({})'.format(url_, sMediaUrl))
                    urlTab.append({'name': 'UrlParser', 'url': url_, 'need_resolve': 1})
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("vStream.getVideoLinks [%s]" % videoUrl)

        return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        otherInfo = {}
        icon = cItem.get('icon', '')
        titre = cItem.get('title', '')
        desc = cItem.get('desc', '')
        sFileName = cItem.get('sFileName', '')
        Year = cItem.get('Year', '')
        sMeta = cItem.get('sMeta', '')
        if sFileName.strip() == '':
            sFileName = titre
        grab = cTMDb()
        elm = grab.get_meta(sMeta, sFileName, year=str(Year))
        duration = elm.get('duration', 0)

        if (duration != '') and (duration != 0):
            try:
                duration = time.strftime('%-Hh %Mmn', time.gmtime(int(duration)))
            except:
                pass
        if (duration != 0) and (duration != ''):
            otherInfo['duration'] = str(duration)
        if elm.get('rating', 0) != 0:
            otherInfo['tmdb_rating'] = str(elm['rating'])
        if elm.get('year', 0) != 0:
            otherInfo['year'] = str(elm['year'])
        if elm.get('writer', '') != '':
            otherInfo['writers'] = str(elm['writer'])
        if elm.get('genre', '') != '':
            otherInfo['genres'] = str(elm['genre'])
        if elm.get('studio', '') != '':
            otherInfo['station'] = str(elm['studio'])
        if elm.get('director', '') != '':
            otherInfo['directors'] = str(elm['director'])
        if elm.get('plot', '') != '':
            desc = '\c00????00'+'Plot: '+'\c0000????'+str(elm['plot'])
        if elm.get('poster_path', '') != '':
            poster_path = str(elm['poster_path'])
            printDBG('poster_path = '+str(poster_path))

            icon = poster_path
        return [{'title': titre, 'text': desc, 'images': [{'title': '', 'url': icon}], 'other_info': otherInfo}]

    def write_search(self, txt='', txt_def=''):
        PathStr = '{}searchSTR'.format(self.MyPath)
        if txt_def == '':
            if os.path.isfile(PathStr):
                with open(PathStr, 'r') as f:
                    txt_def = f.read().strip()
        if txt == '':
            ret = self.sessionEx.waitForFinishOpen(GetVirtualKeyboard(), title=_('Set file name'), text=txt_def)
            input_txt = ret[0]
        else:
            input_txt = txt

        if isinstance(input_txt, ("".__class__, u"".__class__)):
            with open(PathStr, 'w') as file:
                file.write(input_txt)
                file.close()

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        TmdbPath = '{}tmdb'.format(self.MyPath)
        if not os.path.exists(TmdbPath):
            os.makedirs(TmdbPath)
        files = glob.glob('{}/*'.format(TmdbPath))
        for f in files:
            os.remove(f)

        if os.path.exists(self.workflag):
            os.remove(self.workflag)
            GetIPTVSleep().Sleep(5)
        files = glob.glob('{}tmdb/*'.format(self.MyPath))
        for f in files:
            os.remove(f)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')

        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

        # MAIN MENU
        if name == None and category == '':
            self.listMainMenu({'name': 'category'})
        elif category == 'list_items':
            self.listItems(self.currItem)
        else:
            printExc()
        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, vStream(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'list_items':
            return False
        return True
