# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.p2p3.UrlLib import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'MovizTime'


class MovizTime(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'moviztime', 'cookie': 'moviztime.cookie'})

        self.MAIN_URL = 'https://movtime93.homes/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/QNp7dsj/moviztime.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'with_metadata': True, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("MovizTime.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'movei', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'serie', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-أجنبية-مترجمة-d/')},
            {'category': 'anime', 'title': _('الأنـمـي'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'tvshow', 'title': _('برامج تلفزيونية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/برامج-تلفزيونية/')},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG("MovizTime.listCatItems cItem[%s]" % (cItem))
        category = self.currItem.get("category", '')

        if category == 'movei':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('أفــلام أجنـبيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/أفلام-أجنبية/')},
                {'category': 'list_items', 'title': _('أفــلام أوروبـية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/أفلام-أوروبية/')},
                {'category': 'list_items', 'title': _('أفــلام تـركـيـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/أفلام-تركية/')},
                {'category': 'list_items', 'title': _('أفــلام هنــديـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/أفلام-هندية/')},
                {'category': 'list_items', 'title': _('أفــلام آسـيـويـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/أفلام-آسيوية-مترجمة/')},
                {'category': 'list_items', 'title': _('أفــلام وثائـقـية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/أفلام-وثائقية/')},]
        elif category == 'anime':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('أفــلام أنـمـي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/قائمة-الأنمي-b/أفلام-أنمي/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات أنـمـي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/قائمة-الأنمي-b/مسلسلات-أنمي/')},]
        self.listsTab(NEW_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG("MovizTime.listItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'posts-navigation'), ('</div', '>'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''href=['"]([^"^']+/%s/?)['"]''' % (page + 1))[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<section', '>', 'content'), ('</section', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<article', '>', 'pinbox'), ('</a', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])
            desc = ph.extract_desc(item, [('quality', '''_quality_tag['"].+?>([^>]+?)[$<]''')])

            info = ph.std_title(title, desc=desc, with_ep=True)
            if title != '':
                title = info.get('title_display')
            desc = info.get('desc')

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG("MovizTime.exploreItems cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('قصة الفيلم', '>'), ('</p', '>'), False)[1])
        if desc == '':
            desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('قصة المسلسل', '>'), ('</p', '>'), False)[1])

        if 'series' in cItem['url'] or 'anime' in cItem['url']:
            tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'servers-block'), ('<style', '>', 'text/css'), True)[1]
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<button', 'ep-item'), ('</button', '>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('</button', '>'), False)[1])

                if 'kooora' in url:
                    continue

                info = ph.std_title(title, with_ep=True)
                if title != '':
                    title = info.get('title_display')
                otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                self.addVideo(params)
        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("MovizTime.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        url = self.getFullUrl('/search/{}'.format(urllib_quote_plus(searchPattern)))
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG("MovizTime.getLinksForVideo [%s]" % cItem)
        urlTab = []

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return
        cUrl = data.meta['url']

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'single_tab ifa'), ('</iframe', '>'), True)[1]
        url = self.getFullUrl(self.cm.ph.getSearchGroups(tmp, '''data-src=['"]([^"^']+?)['"]''')[0])

        title = ('{} \c00??7950 [{}]\c00??????'.format(cItem['title'], self.up.getHostName(url, True)))
        urlTab.append({'name': title, 'url': strwithmeta(url, {'Referer': cUrl}), 'need_resolve': 1})
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("MovizTime.getVideoLinks [%s]" % videoUrl)

        return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG("MovizTime.getArticleContent [%s]" % cItem)
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<p', '>', 'movie_details_section'), 'shorten_post_url', True)[1]

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('قصة الفيلم', '>'), ('</p', '>'), False)[1])
        if desc == '':
            desc = cItem['desc']

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('تقييم الفيلم', '>'), ('</span', '>'), False)[1])
        if Info != '':
            otherInfo['imdb_rating'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('سنة الإنتاج', '>'), ('</span', '>'), False)[1])
        if Info != '':
            otherInfo['year'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('جودة الفيلم', '>'), ('</span', '>'), False)[1])
        if Info != '':
            otherInfo['quality'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('نوع الفيلم', '>'), ('</span', '>'), False)[1])
        if Info != '':
            otherInfo['genre'] = Info

        return [{'title': cItem['title'], 'text': desc, 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

        # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'movei' or category == 'anime':
            self.listCatItems(self.currItem)
        elif category == 'list_items' or category == 'serie' or category == 'tvshow':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, MovizTime(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
