# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.libs.urlparserhelper import cMegamax
from Plugins.Extensions.IPTVPlayer.p2p3.UrlLib import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc


def GetConfigList():
    optionList = []
    return optionList

def gettytul():
    return 'HwnaTurkya'


class HwnaTurkya(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'hwnaturkya', 'cookie': 'hwnaturkya.cookie'})

        self.MAIN_URL = 'https://hwnaturkya.com/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/CKWt6hH/hwnaturkya.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}
        self.cacheLinks = {}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("HwnaTurkya.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'movei', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'serie', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG("HwnaTurkya.listCatItems cItem[%s]" % (cItem))
        category = self.currItem.get("category", '')

        if category == 'movei':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('أفــلام تـــركــية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/افلام-تركية-مترجمة.html/')},
                {'category': 'list_items', 'title': _('أفــلام مــدبـلجـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/افلام-تركية-مدبلجة.html/')}]
        elif category == 'serie':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('مسـلـسـلات تـــركــية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-تركية-مترجمة.html/')},
                {'category': 'list_items', 'title': _('مسـلـسـلات مــدبـلجـة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/مسلسلات-تركية-مدبلجة.html/')}]
        self.listsTab(NEW_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG("HwnaTurkya.listItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'pagination'), ('</ul', '>'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''href=['"]([^"^']+/page%s?)['"]''' % (page + 1))[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'main-content bg1'), ('<footer', '>', 'bg1'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<article', '>', 'post'), ('</article', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''data-original=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])
            desc = ph.extract_desc(item, [('quality', '''quality['"]>([^>]+?)[$<]'''), ('rating', '''icon-star-full2 f13['"].+?([^>]+?)[$<]''')])

            info = ph.std_title(title, desc=desc, with_ep=True)
            if title != '':
                title = info.get('title_display')
            desc = info.get('desc')

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG("HwnaTurkya.exploreItems cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('<h3', '>', 'story'), ('</h3', '>'), False)[1])

        if '/series/' in cItem['url'] or '/episodes/' in cItem['url']:
            Season = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'getSeasonsBySeries'), ('<div', '>', 'getPostRand'), True)[1]
            if Season:
                self.addMarker({'title': '\c0000??00 مـــواســم', 'icon': cItem['icon'], 'desc': ''})
                tmp = self.cm.ph.getAllItemsBeetwenMarkers(Season, ('<div', '>', 'seasonNum'), ('<div', '>', 'container'))
                for item in tmp:
                    url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                    title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<h3', '>', 'content'), ('</h3', '>'), False)[1])

                    info = ph.std_title(title, with_ep=True)
                    if title != '':
                        title = info.get('title_display')
                    otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                    params = dict(cItem)
                    params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                    self.addDir(params)

            Episod = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'list-episodes'), ('</ul>', '</div>'), True)[1]
            if Episod:
                self.addMarker({'title': '\c0000??00 الـحـلـقـات', 'icon': cItem['icon'], 'desc': ''})
                tmp = self.cm.ph.getAllItemsBeetwenMarkers(Episod, ('<li', '>'), ('</li', '>'))
                for item in tmp:
                    url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                    title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<span', '>'), ('</a', '>'), False)[1])

                    info = ph.std_title(title, with_ep=True)
                    if title != '':
                        title = info.get('title_display')
                    otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                    params = dict(cItem)
                    params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                    self.addVideo(params)
        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("HwnaTurkya.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        url = self.getFullUrl('/search/{}'.format(urllib_quote_plus(searchPattern)))
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG("HwnaTurkya.getLinksForVideo [%s]" % cItem)
        baseUrl = cItem['url'].replace('movies', 'watch_movies').replace('episodes', 'watch_episodes')
        urlTab = []

        sts, data = self.getPage(baseUrl)
        if not sts:
            return

        postID = self.cleanHtmlStr(self.cm.ph.getSearchGroups(data, '''postID = ['"]([^"^']+?)['"]''')[0])
        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'list-serv'), ('</ul', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', '>'), ('</li', '>'))
        for item in tmp:
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('</i', '>'), ('</a', '>'), False)[1])

            post_data = {'server': title, 'postID': postID, 'Ajax': '1'}

            sts, data = self.getPage(self.getFullUrl('/ajax/getPlayer'), post_data=post_data)
            if not sts:
                return
            url = self.getFullUrl(self.cm.ph.getSearchGroups(data, '''SRC=['"]([^"^']+?)['"]''', ignoreCase=True)[0])

            if 'megamax.me' in url:
                for item in cMegamax(url):
                    url = item.split(',')[0].split('=')[1]
                    sQual = item.split(',')[1].split('=')[1]
                    sLabel = item.split(',')[2].split('=')[1]

                    title_ = ('{} \c00??7950 [{}]\c00??????\c00???020 - {}\c00??????'.format(cItem['title'], sQual, sLabel))
                    urlTab.append({'name': title_, 'url': url, 'need_resolve': 1})

            if title != '':
                title = ('{} \c00??7950 [{}]\c00??????\c00???020 - {}\c00??????'.format(cItem['title'], title, self.up.getHostName(url, True)))
            urlTab.append({'name': title, 'url': url, 'need_resolve': 1})

            self.cacheLinks[str(cItem['url'])] = urlTab
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("HwnaTurkya.getVideoLinks [%s]" % videoUrl)

        return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG("HwnaTurkya.getArticleContent [%s]" % cItem)
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'singleInfo'), ('<div', '>', 'single-full'), True)[1]

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('<div', '>', 'the-story'), ('</h3', '>'), False)[1])
        if desc == '':
            desc = cItem['desc']

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, 'IMDB', ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['imdb_rating'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('years', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['year'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('quality', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['quality'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('النوع', '>'), ('</li', '>'), False)[1])
        if Info != '':
            otherInfo['genre'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('الوقت', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['duration'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenMarkers(tmp, ('اللغة', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['language'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenMarkers(tmp, ('دول الأنتاج', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['country'] = Info

        return [{'title': cItem['title'], 'text': desc, 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

        # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'movei' or category == 'serie':
            self.listCatItems(self.currItem)
        elif category == 'list_items':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, HwnaTurkya(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
