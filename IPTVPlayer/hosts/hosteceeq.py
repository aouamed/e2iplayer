# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS

from base64 import b64decode

from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    SetIPTVPlayerLastHostError
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.libs.e2ijson import loads as json_loads
from Plugins.Extensions.IPTVPlayer.p2p3.UrlLib import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc


def GetConfigList():
    optionList = []
    return optionList

def gettytul():
    return 'Esheeq'


class Esheeq(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'eceeq', 'cookie': 'eceeq.cookie'})

        self.MAIN_URL = 'https://eceeq.org/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/nBz6Rfz/esseq.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("Esheeq.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'show_movies', 'title': _('أفـــلام'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/category/الأفلام-التركية/')},
            {'category': 'show_series', 'title': _('مســلســلات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/all-series/')},
            {'category': 'show_movies', 'title': _('آخــر الحـلقـات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/episodes/')},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG("Esheeq.listItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmpNext = self.cm.ph.getDataBeetwenMarkers(data, ('pagination', '>'), ('</div', '>'), True)[1]
        nextPage = self.cm.ph.getDataBeetwenMarkers(tmpNext, ('current', '>'), ('inactive', '>'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''href=['"]([^"^']+?)['"]''')[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('container-fluid', '>'), ('footer', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('block-post', '>'), ('</article', '>'))
        for item in tmp:
            icon = self.cm.ph.stdUrl(self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''url\((.+?)\)''')[0]))
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = ph.decodeHtml(self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<div', '>', 'title', ), ('</div', '>'), True)[1]))

            info = ph.std_title(title, with_type=False, with_ep=True)
            if title != '':
                title = info.get('title_display').replace('“', '').replace('"', '').replace('”', ' ').strip()
            desc = info.get('desc')

            if '?url=' in url:
                tmpUrl = url.split('?url=')[-1].replace('%3D', '=')
                url = b64decode(tmpUrl).decode("utf-8")

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': icon, 'desc': desc})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG("Esheeq.exploreItems cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('<div', '>', 'story'), ('</div', '>'), False)[1])
        if "series" in cItem['url']:

            tmp = self.cm.ph.getDataBeetwenMarkers(data, ('container-fluid', '>'), ('footer', '>'), True)[1]
            msg = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('<div', '>', 'noResult alert alert-dismissable'), ('</div', '>'), False)[1])
            if msg != '':
                SetIPTVPlayerLastHostError(_(msg))
            else:
                tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('block-post', '>'), ('</article', '>'))
                for item in tmp:
                    url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                    title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])

                    if '?url=' in url:
                        tmpUrl = url.split('?url=')[-1]
                        url = b64decode(tmpUrl).decode("utf-8")

                    info = ph.std_title(title, with_ep=True)
                    if title != '':
                        title = info.get('title_display')
                    otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                    params = dict(cItem)
                    params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': self.getFullUrl(url), 'icon': cItem['icon'], 'desc': otherInfo})
                    self.addVideo(params)
        else:

            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("AnimeBlkom.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        url = self.getFullUrl('/search/{}'.format(urllib_quote_plus(searchPattern)))
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG("Esheeq.getLinksForVideo [%s]" % cItem)
        urlTab = []

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'skipAd'), ('</span', '>'), True)[1]
        tmp = self.getFullUrl(self.cm.ph.getSearchGroups(tmp, '''href=['"]([^"^']+?)['"]''')[0])

        tmpUrl = tmp.split('?post=')[-1]
        tmpUrl = json_loads(b64decode(tmpUrl).decode("utf-8"))

        tmpServ = tmpUrl.get('servers')
        for aEntry in tmpServ:
            tmpID = aEntry["id"]
            tmpName = aEntry["name"]
            if 'dailymotion' == tmpName:
                url = 'https://www.dailymotion.com/video/{}'.format(tmpID)
            if 'ok' == tmpName:
                url = 'https://www.ok.ru/videoembed/{}'.format(tmpID)
            if 'tune' == tmpName:
                url = 'https://tune.pk/js/open/embed.js?vid={}&userid=827492&_=1601112672793'.format(tmpID)
            if 'estream' == tmpName:
                url = 'https://arabveturk.com/embed-{}.html'.format(tmpID)
            if 'now' == tmpName:
                url = 'https://extreamnow.org/embed-{}.html'.format(tmpID)
            if 'box' == tmpName:
                url = 'https://youdboox.com/embed-{}.html'.format(tmpID)
            if 'Pro HD' == tmpName:
                url = 'https://segavid.com/embed-{}.html'.format(tmpID)
            if 'Red HD' == tmpName:
                url = 'https://embedwish.com/e/{}'.format(tmpID)
            if 'online' == tmpName:
                url = 'https://player.vimeo.com/video/{}?title=0&byline=0'.format(tmpID)
            if 'youtube' == tmpName:
                url = 'https://www.youtube.com/watch?v={}'.format(tmpID)
            if 'plus' == tmpName:
                url = 'https://tuneplus.co/js/open/embed.js?vid={}&userid=958509&_=1627365111402'.format(tmpID)

            title = ('{} \c00??7950 [{}]\c00??????\c00???020 - {}\c00??????'.format(cItem['title'], tmpName, self.up.getHostName(url, True)))

            urlTab.append({'name': title, 'url': url, 'need_resolve': 1})
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("Esheeq.getVideoLinks [%s]" % videoUrl)

        return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG("Esheeq.getArticleContent [%s]" % cItem)
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('<div', '>', 'story'), ('</div', '>'), False)[1])

        if desc == '':
            desc = cItem['desc']

        return [{'title': cItem['title'], 'text': desc, 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

    # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'show_movies':
            self.listItems(self.currItem)
        elif category == 'show_series':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, Esheeq(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
