# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from base64 import b64decode

from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.libs.e2ijson import loads as json_loads
from Plugins.Extensions.IPTVPlayer.p2p3.manipulateStrings import (
    ensure_str, iterDictItems)
from Plugins.Extensions.IPTVPlayer.p2p3.UrlLib import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'BahyFilm'


class BahyFilm(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'bahyfilm', 'cookie': 'bahyfilm.cookie'})

        self.MAIN_URL = 'https://bahyfilm.com/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/S6N1271/bahyfilm.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'with_metadata': True, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("BahyFilm.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'movei', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG("BahyFilm.listCatItems cItem[%s]" % (cItem))
        category = self.currItem.get("category", '')

        if category == 'movei':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('أفلام أجنبية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/section.php?sidebarID=11')},
                {'category': 'list_items', 'title': _('أفلام أسيوية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/section.php?sidebarID=15')},
                {'category': 'list_items', 'title': _('أفلام هندية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/section.php?sidebarID=12')},
                {'category': 'list_items', 'title': _('افلام اسبانية'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/section.php?sidebarID=14')},
                {'category': 'list_items', 'title': _('أفلام كرتون'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/section.php?sidebarID=8')},]
        self.listsTab(NEW_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG("BahyFilm.listItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'pagination'), ('</div', '>'), True)[1]
        nextPage = self.cm.ph.getSearchGroups(nextPage, '''href=['"]([^'^"]+?)['"][^>]*?>%s<''' % (page + 1))[0]

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'movice'), ('<footer', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'card'), ('</h3>', '</div>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''data-src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<h3', '>', 'movice-name'), ('</h3', '>'), False)[1])
            desc = ph.extract_desc(item, [('rating', '''rate['"].+?([^>]+?)[$<]''')])

            info = ph.std_title(title, desc=desc, with_ep=True)
            if title != '':
                title = info.get('title_display')
            desc = info.get('desc')

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': self.getFullUrl('section.php%s' % nextPage), 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG("BahyFilm.exploreItems cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('movice-story', '<p>'), ('</p', '>'), False)[1])

        params = dict(cItem)
        params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
        self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("BahyFilm.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        url = self.getFullUrl('/?s={}'.format(urllib_quote_plus(searchPattern)))
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG("BahyFilm.getLinksForVideo [%s]" % cItem)
        urlTab = []

        sID = ensure_str(b64decode(cItem['url'].split('?id=')[1]))

        params = dict(self.defaultParams)
        params['header'] = dict(self.AJAX_HEADER)
        params['header']['Origin'] = self.up.getDomain(self.getMainUrl(), False)
        params['header']['Referer'] = cItem['url']

        post_data = {'movie_id': sID, 'bypasscf': '1'}
        sts, data = self.getPage(self.getFullUrl('getlinks.php'), params, post_data)
        if not sts:
            return

        stsObj = json_loads(ph.decodeHtml(data))
        for sResolution, sLink in iterDictItems(stsObj):
            title = ('{} \c00??7950 [{}p]\c00??????\c00???020 - {}\c00??????'.format(cItem['title'], sResolution, self.up.getHostName(sLink, True)))

            urlTab.append({'name': title, 'url': strwithmeta(self.cm.ph.stdUrl(sLink), {'Referer': self.cm.meta['url']}), 'need_resolve': 0})

        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("BahyFilm.getVideoLinks [%s]" % videoUrl)

        if self.cm.isValidUrl(videoUrl):
            return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem, data=None):
        printDBG("BahyFilm.getArticleContent [%s]" % cItem)
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'movie-details'), ('<div', '>', 'download-details'), True)[1]

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('سنة الاصدار', '<td>'), ('</td', '>'), False)[1])
        if Info != '':
            otherInfo['year'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('اللغة', '<td>'), ('</td', '>'), False)[1])
        if Info != '':
            otherInfo['language'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('<td', '>', 'imdb-film'), ('</td', '>'), False)[1])
        if Info != '':
            otherInfo['imdb_rating'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('النوع', '<td>'), ('</td', '>'), False)[1])
        if Info != '':
            otherInfo['genre'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('البلد', '<td>'), ('</td', '>'), False)[1])
        if Info != '':
            otherInfo['country'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('مدة العرض', '<td>'), ('</td', '>'), False)[1])
        if Info != '':
            otherInfo['duration'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('الجودة', '<td>'), ('</td', '>'), False)[1])
        if Info != '':
            otherInfo['quality'] = Info

        return [{'title': cItem['title'], 'text': cItem['desc'], 'images': [{'title': '', 'url': self.getFullUrl(cItem['icon'])}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

    # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'movei':
            self.listCatItems(self.currItem)
        elif category == 'list_items':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, BahyFilm(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
