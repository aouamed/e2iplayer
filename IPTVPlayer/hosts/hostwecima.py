# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.p2p3.manipulateStrings import escape_str
from Plugins.Extensions.IPTVPlayer.p2p3.UrlLib import (urllib_quote_plus,
                                                       urllib_unquote_plus)
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'WeCima'


class WeCima(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'wecima', 'cookie': 'wecima.cookie'})

        self.MAIN_URL = 'https://weciimaa.online/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/hy6T62x/wecima.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}
        self.cacheLinks = {}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("WeCima.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'movei', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'serie', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'anime', 'title': _('أنــمـــي'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'tvshow', 'title': _('بـــرامج'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG("WeCima.listCatItems cItem[%s]" % (cItem))
        category = self.currItem.get("category", '')

        sts, data = self.getPage(self.getMainUrl())
        if not sts:
            return

        if category == 'movei':
            sStart = '>أفلام'
        elif category == 'serie':
            sStart = '>مسلسلات'
        elif category == 'anime':
            sStart = '>إنيمي و كرتون'
        elif category == 'tvshow':
            sStart = '> المزيد'

        tmp = self.cm.ph.getDataBeetwenMarkers(data, sStart, ('</ul', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, '<li', ('</li', '>'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, '>', '</a>', False)[1])

            if category == 'serie' or title == 'برامج تليفزيونية':
                url += 'list/'

            params = dict(cItem)
            params.update({'category': 'list_items', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': ''})
            self.addDir(params)

        if category == 'serie':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('Netflix'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/production/netflix/list/')},
                {'category': 'list_items', 'title': _('Warner Bros'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/production/warner-bros/list/')},
                {'category': 'list_items', 'title': _('Lionsgate'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/production/lionsgate/list/')},
                {'category': 'list_items', 'title': _('Walt Disney Pictures'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/production/walt-disney-pictures/list/')},]
        elif category == 'movei':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('Columbia Pictures'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/production/columbia-pictures/')},
                {'category': 'list_items', 'title': _('Universal Pictures'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/production/universal-pictures/')},
                {'category': 'list_items', 'title': _('Walt Disney Pictures'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/production/walt-disney-pictures/')}]
        self.listsTab(NEW_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG("WeCima.listItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)
        baseUrl = urllib_unquote_plus(cItem['url'])

        if 'افلام' in baseUrl or 'مصارعة' in baseUrl or baseUrl.endswith('pictures/') or cItem['type'] == 'movies':
            category = 'showMovies'
        elif 'مسلسلات' in baseUrl or baseUrl.endswith('/list/') or cItem['type'] == 'series' or cItem['type'] == 'animes':
            category = 'showSeries'

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'pagination'), ('</ul', '>'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''href=['"]([^'^"]+?)['"][^>]*?>%s<''' % (page + 1))[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'Grid--WecimaPosts'), ('</wecima', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'Thumb--GridItem'), ('<ul', '>', 'PostItemStats'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''url\((.+?)\)''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('hasyear', '>'), ('</strong', '>'), False)[1])

            info = ph.std_title(title, with_ep=True)
            if title != '':
                title = info.get('title_display')
            desc = info.get('desc')

            if not url.startswith(self.MAIN_URL):
                url = url.replace(url.split('watch/')[0], self.MAIN_URL)
            if not icon.startswith(self.MAIN_URL):
                icon = icon.replace(icon.split('wp-content/')[0], self.MAIN_URL)

            params = dict(cItem)
            params.update({'category': category, 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def showMovies(self, cItem):
        printDBG("WeCima.showMovies cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('<div', '>', 'StoryMovieContent'), ('<div', '>'), False)[1])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<i', '>', 'fa-camera-movie'), ('</singlerelated', '>'), True)[1]
        if tmp:
            self.addMarker({'title': '{}الــــفــلـم'.format('\c0000??00'), 'icon': cItem['icon'], 'desc': ''})
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

            self.addMarker({'title': '{}الــسلــسلـة'.format('\c0000??00'), 'icon': cItem['icon'], 'desc': ''})
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<div', '>', 'GridItem'), ('<ul', '>', 'PostItemStats'))
            for item in tmp:
                icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''url\((.+?)\)''')[0])
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])

                info = ph.std_title(title, with_ep=True)
                if title != '':
                    title = info.get('title_display')
                otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                params = dict(cItem)
                params.update({'category': 'showMovies', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': icon, 'desc': otherInfo})
                self.addDir(params)
        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def showSeries(self, cItem):
        printDBG("WeCima.showSeries cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('<div', '>', 'PostItemContent'), ('<div', '>'), False)[1])
        tmpUrl = self.cm.ph.getDataBeetwenMarkers(data, ('<i', '>', 'far fa-tv-retro'), ('</singlesections', '>'), True)[1]

        nextPage = self.cm.ph.getDataBeetwenMarkers(tmpUrl, 'MoreEpisodes--Button', ('</span', '>'), True)[1]
        nextPage = self.cm.ph.getSearchGroups(nextPage, '''data-term=['"]([^"^']+?)['"]''')[0]

        Season = self.cm.ph.getDataBeetwenMarkers(tmpUrl, ('<div', '>', 'List--Seasons--Episodes'), ('</div', '>'), True)[1]
        if Season:
            self.addMarker({'title': '{}مـــواســم'.format('\c0000??00'), 'icon': cItem['icon'], 'desc': ''})
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(Season, '<a', ('</a', '>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, '>', ('</a', '>'), False)[1])

                info = ph.std_title(title)
                if title != '':
                    title = info.get('title_display')
                otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                self.addDir(params)

        Episod = self.cm.ph.getDataBeetwenMarkers(tmpUrl, ('<div', '>', 'Episodes--Seasons--Episodes'), ('</singlesection', '>'), True)[1]
        if Episod:
            self.addMarker({'title': '{}الـحـلـقـات'.format('\c0000??00'), 'icon': cItem['icon'], 'desc': ''})
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(Episod, ('<a', '>', 'hoverable activable'), ('</episodeArea', '>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<episodeTitle', '>'), ('</episodeTitle', '>'), False)[1])

                info = ph.std_title(title, with_ep=True)
                if title != '':
                    title = info.get('title_display')
                otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                self.addVideo(params)

            if nextPage != '':
                params = dict(cItem)
                params.update({'category': 'showEps', 'title': _("Next page"), 'url': nextPage})
                self.addMore(params)

    def showEps(self, cItem):
        printDBG("WeCima.showEps cItem[%s]" % (cItem))

        baseUrl = 'https://mycima.biz/AjaxCenter/MoreEpisodes/%s' % cItem['url']
        for sItem in ('30', '70', '110'):
            sts, data = self.getPage('%s/%s/' % (baseUrl, sItem))
            if not sts:
                return

            tmp = escape_str(ph.decodeHtml(data))
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<a', '>', 'hoverable activable'), ('</episodeArea', '>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<episodeTitle', '>'), ('</episodeTitle', '>'), False)[1])

                info = ph.std_title(title, with_ep=True)
                if title != '':
                    title = info.get('title_display')
                desc = info.get('desc')

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': desc})
                self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("Aflaam.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))

        if searchType == 'movies':
            url = self.getFullUrl('/search/{}'.format(urllib_quote_plus(searchPattern)))
        elif searchType == 'series':
            url = self.getFullUrl('/search/{}/list/series/'.format(urllib_quote_plus(searchPattern)))
        elif searchType == 'animes':
            url = self.getFullUrl('/search/{}/list/anime/'.format(urllib_quote_plus(searchPattern)))
        params = {'name': 'category', 'category': 'list_items', 'type': searchType, 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG("WeCima.getLinksForVideo [%s]" % cItem)
        urlTab = []

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'WatchServersList'), ('<div', '>', 'WatchServersEmbed'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', '>'), ('</li', '>'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''data-url=['"]([^"^']+?)['"]''')[0]).replace('\\', '/')
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<strong', '>'), ('</strong', '>'), False)[1])

            if title != '':
                title = ('{} \c00??7950 [{}]\c00??????\c00???020 - {}\c00??????'.format(cItem['title'], title, self.up.getHostName(url, True)))

            urlTab.append({'name': title, 'url': url, 'need_resolve': 1})

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'List--Download--Wecima--Single'), ('</singlesection', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', '>'), ('</li', '>'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0]).replace('\\', '/')
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<resolution', '>'), ('</resolution', '>'), False)[1])

            if title != '':
                title = ('{} \c00??7950 [{}]\c00??????\c00???020 - {}\c00??????'.format(cItem['title'], title, self.up.getHostName(url, True)))

            urlTab.append({'name': title, 'url': url, 'need_resolve': 1})

            self.cacheLinks[str(cItem['url'])] = urlTab
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("WeCima.getVideoLinks [%s]" % videoUrl)

        return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG("WeCima.getArticleContent [%s]" % cItem)
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('<div', '>', 'StoryMovieContent'), ('<div', '>'), False)[1])
        if desc == '':
            desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('<div', '>', 'PostItemContent'), ('<div', '>'), False)[1])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'Title--Content--Single-begin'), ('<div', '>', 'AsideContext'), True)[1]

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, 'المدة', ('</p', '>'), False)[1])
        if Info != '':
            otherInfo['duration'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, 'الجودة', ('</p', '>'), False)[1])
        if Info != '':
            otherInfo['quality'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, 'النوع', ('</p', '>'), False)[1])
        if Info != '':
            otherInfo['genre'] = Info

        if desc == '':
            desc = cItem['desc']

        return [{'title': cItem['title'], 'text': desc, 'images': [{'title': '', 'url': self.getFullUrl(cItem['icon'])}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

    # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'movei' or category == 'serie' or category == 'anime' or category == 'tvshow':
            self.listCatItems(self.currItem)
        elif category == 'list_items':
            self.listItems(self.currItem)
        elif category == 'showMovies':
            self.showMovies(self.currItem)
        elif category == 'showSeries':
            self.showSeries(self.currItem)
        elif category == 'showEps':
            self.showEps(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, WeCima(), True, [])

    def getSearchTypes(self):
        searchTypesOptions = []
        searchTypesOptions.append(("Movies", "movies"))
        searchTypesOptions.append(("TV Shows", "series"))
        searchTypesOptions.append(("Animes", "animes"))
        return searchTypesOptions

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
