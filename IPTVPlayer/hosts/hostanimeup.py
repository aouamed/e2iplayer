# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.libs.urlparserhelper import cMegamax
from Plugins.Extensions.IPTVPlayer.p2p3.UrlLib import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc


def GetConfigList():
    optionList = []
    return optionList

def gettytul():
    return 'Anime4up'


class Anime4up(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'anime4up', 'cookie': 'anime4up.cam.cookie'})

        self.MAIN_URL = 'https://anime4up.cam/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/0K6TfBL/animeup.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}
        self.cacheLinks = {}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("Anime4up.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'show_list', 'title': _('أحدث الأفلام'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/anime-type/movie-3')},
            {'category': 'show_list', 'title': _('مسلسلات انمي'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/anime-season/خريف-2023/')},
            {'category': 'show_list', 'title': _('انمي مدبلج'), 'icon': self.DEFAULT_ICON_URL, 'url':  self.getFullUrl('/anime-category/الانمي-المدبلج/')},
            {'category': 'show_list', 'title': _('انمي مترجم'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/anime-category/الانمي-المترجم/')},
            {'category': 'show_list', 'title': _('يعرض الان'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/episode/')},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG("Anime4up.listItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<link', 'next'), ('/>', '<meta'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''href=['"]([^"^']+?)['"]''')[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('anime-list-content', '>'), ('site-copyrights', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<img', 'img-responsive'), ('anime-card-container', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''alt=['"]([^"^']+?)['"]''')[0])
            desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('data-content', '='), ('>', '<h3>'), False)[1])

            info = ph.std_title(title, with_ep=True)
            if title != '':
                title = info.get('title_display')
            otherInfo = '{}\n{}'.format(info.get('desc'), desc)

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': otherInfo})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG("Anime4up.exploreItems cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        # trailer
        trailerUrl = self.cm.ph.getDataBeetwenMarkers(data, '<a target=', 'class="anime-trailer">')[1]
        trailerUrl = self.cm.ph.getSearchGroups(trailerUrl, '''href=['"]([^"^']+?)['"]''')[0]
        if self.cm.isValidUrl(trailerUrl):
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': '[\c00???020Trailer\c00??????]', 'url': trailerUrl, 'desc': ''})
            self.addVideo(params)

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('display-flex', '>'), ('space', '></div>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<img', 'img-responsive'), ('overlay', '></a>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''alt=['"]([^"^']+?)['"]''')[0])

            info = ph.std_title(title, with_ep=True)
            if title != '':
                title = info.get('title_display')
            desc = info.get('desc')

            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("Anime4up.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        url = self.getFullUrl('/?search_param=animes&s={}'.format(urllib_quote_plus(searchPattern)))
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG("Anime4up.getLinksForVideo [%s]" % cItem)
        urlTab = []

        if 'Trailer' in cItem['title']:
            return self.up.getVideoLinkExt(cItem['url'])

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('episode-download-container', '>'), ('episode-anime-section', '>'), True)[1]

        url = self.getFullUrl(self.cm.ph.getSearchGroups(tmp, '''dw-online['"].+?href=['"]([^"^']+?)['"]''')[0])

        if 'megamax' in url:
            for item in cMegamax(url):
                sHosterUrl = item.split(',')[0].split('=')[1]
                sQual = item.split(',')[1].split('=')[1]
                sLabel = item.split(',')[2].split('=')[1]

                title = ('{} \c00??7950 [{}]\c00??????\c00???020 - {}\c00??????'.format(ph.std_title(cItem['title'], with_ep=True)['title_display'], sQual, sLabel))
                urlTab.append({'name': title, 'url': sHosterUrl, 'need_resolve': 1})

        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('quality-list', '>'), '</ul>')
        for item in tmp:
            quality = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''<li>([^>]+?)</''')[0])

            tmp = self.cm.ph.getAllItemsBeetwenMarkers(item, 'btn-default', '</a>')
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''href.+?['"]>([^>]+?)</''')[0])

                if title != '':
                    title = ('{} \c00??7950 [{}]\c00??????\c00???020 - {}\c00??????'.format(ph.std_title(cItem['title'], with_ep=True)['title_display'], quality, title))

                urlTab.append({'name': title, 'url': url, 'need_resolve': 1})

            self.cacheLinks[str(cItem['url'])] = urlTab
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("Anime4up.getVideoLinks [%s]" % videoUrl)

        return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG("Anime4up.getArticleContent [%s]" % cItem)
        itemsList = []

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('anime-details', '>'), ('clearfix', '</div>'), True)[1]

        title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('anime-details-title', '>'), '</h1>', False)[1])
        if title == '':
            title = cItem['title']

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('anime-story', '>'), ('</p>', '<div'), False)[1])
        if desc == '':
            desc = cItem['desc']

        itemsList.append((_('Genres'), self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('<a', '>'), ('</a>', '</li>'))[1])))

        return [{'title': self.cleanHtmlStr(title), 'text': self.cleanHtmlStr(desc), 'images': [{'title': '', 'url': self.getFullUrl(cItem['icon'])}], 'other_info': {'custom_items_list': itemsList}}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

        # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'show_list':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, Anime4up(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
