# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS

from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc


def GetConfigList():
    optionList = []
    return optionList

def gettytul():
    return 'CartoonArbi'


class CartoonArbi(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'cookie': 'arteenz.cookie'})

        self.MAIN_URL = 'https://www.arteenz.com/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/6nsRSXN/cartoonrbi.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("CartoonArbi.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'show_movies', 'title': _('أفـــلام'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/films.html')},
            {'category': 'show_series', 'title': _('مســلســلات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/cats.html')}]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG("CartoonArbi.listItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('current', '>'), ('</a>', '<a href'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''href=['"]([^"^']+?)['"]''')[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('blocks2', '>'), ('footer', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('cartoon_cat_pic', '>'), ('cartoon_cat', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])

            info = ph.std_title(title, with_ep=True)
            if title != '':
                title = info.get('title_display')
            desc = info.get('desc')

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': self.getFullUrl(nextPage), 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG("CartoonArbi.exploreItems cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        if "cartooncat" in cItem['url'] or "cartoonpagecat" in cItem['url']:
            page = cItem.get('page', 1)

            nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('current', '>'), ('<div', 'style', '>'), True)[1]
            nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''href=['"]([^"^']+?)['"]''')[0])

            tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', 'blocks', '>'), ('pagination', '>'), True)[1]
            desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('block_body_story', '>'), ('</div', '>'), False)[1])

            tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('cartoon_eps_pic', '>'), ('cartoon_eps_name', '>'))
            for item in tmp:
                icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+?)['"]''')[0])
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])

                info = ph.std_title(title, with_ep=True)
                if title != '':
                    title = info.get('title_display')
                otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': otherInfo})
                self.addVideo(params)

            if nextPage != '':
                params = dict(cItem)
                params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
                self.addDir(params)
        else:

            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': ''})
            self.addVideo(params)

    def getLinksForVideo(self, cItem):
        printDBG("CartoonArbi.getLinksForVideo [%s]" % cItem)
        urlTab = []
        sUrl = cItem['url'].replace("cartoon", "watch-")

        sts, data = self.getPage(sUrl)
        if not sts:
            return
        sName = self.getFullUrl(self.cm.ph.getSearchGroups(data, '''data.+?['"]&p=([^=]+?)&''')[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('servers', '>'), ('videoshow', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, '<li', '</li>')
        for item in tmp:
            sErver = self.cm.ph.getSearchGroups(item, '''server_ch\((.+?),''')[0]
            sPage = self.cm.ph.getSearchGroups(item, '''server_ch.+?,['"]([^"^']+?)['"]''')[0]
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-cf-modified.+>([^>]+?)</''')[0])
            siteUrl = self.getFullUrl('/plugins/server{}/embed.php?url={}&id={}'.format(sErver, sPage, sName))

            sts, data = self.getPage(siteUrl)
            if not sts:
                return

            tmp = self.cm.ph.getDataBeetwenMarkers(data, '<iframe', '</iframe>', True)[1]
            url = self.getFullUrl(self.cm.ph.getSearchGroups(tmp, '''src=['"]([^"^']+?)['"]''')[0])

            if title != '':
                title = ('{} \c00??7950 [{}]\c00??????\c00???020 - {}\c00??????'.format(ph.std_title(cItem['title'], with_ep=True)['title_display'], title, self.up.getHostName(url, True)))

            urlTab.append({'name': title, 'url': url, 'need_resolve': 1})
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("CartoonArbi.getVideoLinks [%s]" % videoUrl)

        return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG("CartoonArbi.getArticleContent [%s]" % cItem)
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('ico_story', '>'), ('ico_download', '>'), True)[1]
        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('style', '>'), '</div>', False)[1])

        title = cItem['title']
        if desc == '':
            desc = cItem['desc']

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('game_name_c', '>'), ('game_name_n esp_tag', '>'), True)[1]

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('esp_vis', '>'), '</div>', False)[1])
        if Info != '':
            otherInfo['status'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('esp_time', '>'), '</div>', False)[1])
        if Info != '':
            otherInfo['duration'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('esp_date', '>'), '</div>', False)[1])
        if Info != '':
            otherInfo['year'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('esp_cat', '>'), '</div>', False)[1])
        if Info != '':
            otherInfo['type'] = Info

        return [{'title': self.cleanHtmlStr(title), 'text': self.cleanHtmlStr(desc), 'images': [{'title': '', 'url': self.getFullUrl(cItem['icon'])}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

        # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'show_movies':
            self.listItems(self.currItem)
        elif category == 'show_series':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, CartoonArbi(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
