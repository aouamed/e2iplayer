# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.libs.urlparserhelper import cMegamax
from Plugins.Extensions.IPTVPlayer.p2p3.UrlLib import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc


def GetConfigList():
    optionList = []
    return optionList

def gettytul():
    return 'Shahiid Anime'


class ShahiidAnime(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'shahiidanimenet', 'cookie': 'shahiidanime.cookie'})

        self.MAIN_URL = 'https://shahiid-anime.net/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/nfCx7PD/Shahiid-Anime.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'Accept': '*/*', 'X-Requested-With': 'XMLHttpRequest', 'Connection': 'keep-alive', 'Accept-Encoding': 'gzip', 'Pragma': 'no-cache'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}
        self.cacheLinks = {}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("ShahiidAnime.listMainMenu")

        MAIN_CAT_TAB = [
            {'category': 'show_list', 'title': _('الافــلام'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/anime/')},
            {'category': 'show_list', 'title': _('مســلــسـلات'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/series/')},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listItems(self, cItem):
        printDBG("ShahiidAnime.listItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, 'rel="next"', '/>', True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''href=['"]([^"^']+?)['"]''')[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('page-header', '>'), ('foot-menu', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('wrap-poster clearfix', '>'), ('one-poster', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''src=['"]([^"^']+\.jpe?g)''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''<a href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenMarkers(item, '<h2>', '</h2>', True)[1])
            desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('story-text', '>'), '</p>', False)[1])
            other = ph.extract_desc(item, [('rating', '''fa fa-star['"].+?([^>]+?)[$<]''')])

            info = ph.std_title(title, desc=other, with_ep=True)
            if title != '':
                title = info.get('title_display')
            otherInfo = '{}\n{}'.format(info.get('desc'), desc)

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': otherInfo})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG("ShahiidAnime.exploreItems cItem[%s]" % (cItem))

        if '/series/' in cItem['url']:
            params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': cItem['url'], 'icon': cItem['icon'], 'desc': cItem['desc']}
            self.showItems(params)
        else:

            sts, data = self.getPage(cItem['url'])
            if not sts:
                return

            # trailer
            trailerUrl = self.cm.ph.getDataBeetwenMarkers(data, ('thumb-btn-trailer', '>'), ('fab fa-youtube', '>'))[1]
            trailerUrl = self.cm.ph.getSearchGroups(trailerUrl, '''href=['"]([^"^']+?)['"]''')[0]
            if self.cm.isValidUrl(trailerUrl):
                params = dict(cItem)
                params.update({'good_for_fav': False, 'title': '[\c00???020Trailer\c00??????]', 'url': trailerUrl, 'desc': ''})
                self.addVideo(params)

            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': cItem['desc']})
            self.addVideo(params)

    def showItems(self, cItem):
        printDBG("ShahiidAnime.showItems cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        url = self.getFullUrl(self.cm.ph.getSearchGroups(data, '''window.location = ['"]([^"^']+?)['"]''')[0])

        sts, data = self.getPage(url)
        if not sts:
            return

        # trailer
        trailerUrl = self.cm.ph.getDataBeetwenMarkers(data, ('thumb-btn-trailer', '>'), ('fab fa-youtube', '>'))[1]
        trailerUrl = self.cm.ph.getSearchGroups(trailerUrl, '''href=['"]([^"^']+?)['"]''')[0]
        if self.cm.isValidUrl(trailerUrl):
            params = dict(cItem)
            params.update({'good_for_fav': False, 'title': '[\c00???020Trailer\c00??????]', 'url': trailerUrl, 'desc': ''})
            self.addVideo(params)

        siteUrl = self.cm.ph.getDataBeetwenMarkers(data, ('wrap-poster clearfix', '>'), ('poster-views', '>'))[1]
        siteUrl = self.cm.ph.getSearchGroups(siteUrl, '''href=['"]([^"^']+?)['"]''')[0]

        if siteUrl:
            params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': siteUrl, 'icon': cItem['icon'], 'desc': cItem['desc']}
            self.showEp(params)

    def showEp(self, cItem):
        printDBG("ShahiidAnime.showEp cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        url = self.getFullUrl(self.cm.ph.getSearchGroups(data, '''window.location = ['"]([^"^']+?)['"]''')[0])

        sts, data = self.getPage(url)
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('page-box clearfix', '>'), ('navigation', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<nav', '>', 'navbar navbar-dark'), ('<i', '>', 'fa fa-download'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('href', '>'), ('</a', '>'), False)[1])

            info = ph.std_title(title, with_ep=True)
            if title != '':
                title = info.get('title_display')
            desc = info.get('desc')

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': self.getFullUrl(url), 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("ShahiidAnime.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        url = self.getFullUrl('/?s={}'.format(urllib_quote_plus(searchPattern)))
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG("ShahiidAnime.getLinksForVideo [%s]" % cItem)
        urlTab = []

        if 'Trailer' in cItem['title']:
            return self.up.getVideoLinkExt(cItem['url'])

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        ajaxurl = self.getFullUrl(self.cm.ph.getSearchGroups(data, '''var ajaxurl = ['"]([^"^']+?)['"]''')[0])
        action = self.cleanHtmlStr(self.cm.ph.getSearchGroups(data, '''['"]action['"]:['"]([^"^']+?)['"]''')[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('tabs-ul nav-justified clearfix', '>'), ('tabs-cntn', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, '<li', '</li>')
        for item in tmp:
            serv = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-serv=['"]([^"^']+?)['"]''')[0])
            frameserver = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-frameserver=['"]([^"^']+?)['"]''')[0])
            post = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-post=['"]([^"^']+?)['"]''')[0])
            title_ = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, '</i>', '</a>', False)[1])

            if 'iframe' in frameserver:
                post_data = {'action': action, 'post': post, 'frameserver': frameserver, 'is_film': 'yes', 'serv': serv}
            else:
                post_data = {'action': action, 'post': post, 'frameserver': frameserver, 'serv': serv}

            sts, data = self.getPage(ajaxurl, post_data=post_data)
            if not sts:
                return

            url = self.getFullUrl(self.cm.ph.getSearchGroups(data, '''src=['"]([^"^']+?)['"]''')[0])
            if 'megamax' in url:
                sHosterUrl = url.replace('/d/', '/f/')
                for item in cMegamax(sHosterUrl):
                    sHosterUrl = item.split(',')[0].split('=')[1]
                    sQual = item.split(',')[1].split('=')[1]
                    sLabel = item.split(',')[2].split('=')[1]

                    title = ('{} \c00??7950 [{}]\c00??????\c00???020 - {}\c00??????'.format(ph.std_title(cItem['title'], with_ep=True)['title_display'], sQual, sLabel))
                    urlTab.append({'name': title, 'url': sHosterUrl, 'need_resolve': 1})

            if title_ != '':
                title_ = ('{} \c00???020 - {}\c00??????'.format(ph.std_title(cItem['title'], with_ep=True)['title_display'], title_))

            if 'megamax' in url:
                continue

            urlTab.append({'name': title_, 'url': url, 'need_resolve': 1})

        self.cacheLinks[str(cItem['url'])] = urlTab
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("ShahiidAnime.getVideoLinks [%s]" % videoUrl)

        return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG("ShahiidAnime.getArticleContent [%s]" % cItem)
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('head-s-movie-cntn', '>'), ('head-s-meta-end', '>'), True)[1]

        title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, '<h1>', '</h1>', False)[1])
        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('head-s-story', '>'), ('</p>', '</div>'), False)[1])

        title = ph.std_title(title, with_ep=True)['title_display']

        if title == '':
            title = cItem['title']
        if desc == '':
            desc = cItem['desc']

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('fa-star', '<span>'), '</span>', False)[1])
        if Info != '':
            otherInfo['imdb_rating'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('fa-clock-o', '<span>'), '</span>', False)[1])
        if Info != '':
            otherInfo['duration'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('fa-calendar-o', 'tag">'), '</span>', False)[1])
        if Info != '':
            otherInfo['year'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('fa-low-vision', 'tag">'), '</span>', False)[1])
        if Info != '':
            otherInfo['quality'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('fa-umbrella', 'tag">'), '</span>', False)[1])
        if Info != '':
            otherInfo['status'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('movies-cats', 'tag">'), '</span>', False)[1])
        if Info != '':
            otherInfo['genre'] = Info

        return [{'title': self.cleanHtmlStr(title), 'text': self.cleanHtmlStr(desc), 'images': [{'title': '', 'url': self.getFullUrl(cItem['icon'])}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

        # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'show_list':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, ShahiidAnime(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
