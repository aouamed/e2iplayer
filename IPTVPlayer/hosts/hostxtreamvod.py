# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS

from datetime import datetime
from os.path import isfile
from re import DOTALL, findall

from Components.config import ConfigText, config, getConfigListEntry
from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.libs.e2ijson import loads as json_loads
from Plugins.Extensions.IPTVPlayer.p2p3.manipulateStrings import ensure_str
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc
from Plugins.Extensions.IPTVPlayer.tools.iptvtypes import strwithmeta

config.plugins.iptvplayer.ts_xtream_user = ConfigText(default='', fixed_size=False)
config.plugins.iptvplayer.ts_xtream_pass = ConfigText(default='', fixed_size=False)
config.plugins.iptvplayer.ts_xtream_host = ConfigText(default='', fixed_size=False)
config.plugins.iptvplayer.ts_xtream_ua = ConfigText(default='', fixed_size=False)


def GetConfigList():
    optionList = []
    optionList.append(getConfigListEntry(_("    Xtream User:"), config.plugins.iptvplayer.ts_xtream_user))
    optionList.append(getConfigListEntry(_("    Xtream Pass:"), config.plugins.iptvplayer.ts_xtream_pass))
    optionList.append(getConfigListEntry(_("    Xtream Host:"), config.plugins.iptvplayer.ts_xtream_host))
    optionList.append(getConfigListEntry(_("    Xtream User Agent:"), config.plugins.iptvplayer.ts_xtream_ua))
    return optionList


def gettytul():
    return 'Xtream \c0000????VOD\c00??????'


class XtreamVod(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'xtreamvod', 'cookie': 'xtreamvod.cookie'})

        self.MAIN_URL = ''
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/LYGhL5v/xtream.png'

        self.HEADER = {'User-Agent': self.cm.getDefaultUserAgent(browser='mobile'), 'X-Requested-With': 'com.sportstv20.app', 'Referer': self.getMainUrl(), 'Origin': self.getMainUrl()}
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def getXtreamConf(self):
        multi_tab = []
        User = config.plugins.iptvplayer.ts_xtream_user.value
        Pass = config.plugins.iptvplayer.ts_xtream_pass.value
        Host = config.plugins.iptvplayer.ts_xtream_host.value
        xUa = config.plugins.iptvplayer.ts_xtream_ua.value
        if ((User != '') and (Pass != '') and (Host != '')):
            name_ = '%s (%s)' % (Host, User)
            if not Host.startswith('http'):
                Host = 'http://%s' % Host
            multi_tab.append((name_, Host, User, Pass, xUa))

        if isfile('/etc/tsiplayer_xtream.conf'):
            with open('/etc/tsiplayer_xtream.conf') as f:
                for line in f:
                    line = line.strip()
                    name_, ua_, host_, user_, pass_ = '', '', '', '', ''
                    _data = findall('(.*?//.*?)/.*?username=(.*?)&.*?password=(.*?)&', line, DOTALL)
                    if _data:
                        name_, host_, user_, pass_ = _data[0][0]+' ('+_data[0][1]+')', _data[0][0], _data[0][1], _data[0][2]
                    else:
                        _data = findall('(.*?)#(.*?)#(.*?)#(.*?)#(.*)', line, DOTALL)
                        if _data:
                            name_, host_, user_, pass_, ua_ = _data[0][0], _data[0][1], _data[0][2], _data[0][3], _data[0][4]
                        else:
                            _data = findall('(.*?)#(.*?)#(.*?)#(.*)', line, DOTALL)
                            if _data:
                                name_, host_, user_, pass_ = _data[0][0], _data[0][1], _data[0][2], _data[0][3]
                    if ((user_ != '') and (pass_ != '') and (host_ != '')):
                        if not host_.startswith('http'):
                            host_ = 'http://'+host_
                        multi_tab.append((name_, host_, user_, pass_, ua_))
        return multi_tab

    def listMainMenu(self, cItem):
        printDBG("XtreamVod.listMainMenu")
        multi_tab = self.getXtreamConf()
        if len(multi_tab) == 0:
            self.addMarker({'title': 'Please configure xstream first', 'icon': self.DEFAULT_ICON_URL, 'desc': 'Please configure xstream first, (add user,pass &host in tsiplayer params or add your config file in /etc/tsiplayer_xtream.conf)'})
        elif len(multi_tab) == 1:
            params = dict(cItem)
            params.update({'icon': self.DEFAULT_ICON_URL, 'xuser': multi_tab[0][2], 'xpass': multi_tab[0][3], 'xhost': multi_tab[0][1], 'xua': multi_tab[0][4]})
            self.listItems(params)
        else:
            for item in multi_tab:
                params = dict(cItem)
                params.update({'category': 'listItems', 'title': item[0], 'icon': self.DEFAULT_ICON_URL, 'xuser': item[2], 'xpass': item[3], 'xhost': item[1], 'xua': item[4]})
                self.addDir(params)

    def listItems(self, cItem):
        printDBG("XtreamVod.listItems cItem[%s]" % (cItem))

        MAIN_CAT_TAB = [
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

        try:
            Url = '%s/player_api.php?username=%s&password=%s&action=get_vod_categories' % (cItem['xhost'], cItem['xuser'], cItem['xpass'])
            sts, data = self.getPage(Url)
            if not sts:
                return
            self.addMarker({'title': '\c0000??00Movei', 'icon': cItem['icon'], 'desc': ''})
            tmp = json_loads(data)
            params = dict(cItem)
            params.update({'category': 'movei', 'title': 'All', 'icon': cItem['icon'], 'category_id': '', 'xuser': cItem['xuser'], 'xpass': cItem['xpass'], 'xhost': cItem['xhost'], 'xua': cItem['xua']})
            self.addDir(params)
            for item in tmp:
                params = dict(cItem)
                params.update({'category': 'movei', 'title': item['category_name'].strip(), 'icon': cItem['icon'], 'category_id': item['category_id'], 'xuser': cItem['xuser'], 'xpass': cItem['xpass'], 'xhost': cItem['xhost'], 'xua': cItem['xua']})
                self.addDir(params)
        except Exception:
            printExc('Cannot parse received data !')

        try:
            Url = '%s/player_api.php?username=%s&password=%s&action=get_series_categories' % (cItem['xhost'], cItem['xuser'], cItem['xpass'])
            sts, data = self.getPage(Url)
            if not sts:
                return
            self.addMarker({'title': '\c0000??00Series', 'icon': cItem['icon'], 'desc': ''})
            tmp = json_loads(data)
            params = dict(cItem)
            params.update({'category': 'serie', 'title': 'All', 'icon': cItem['icon'], 'category_id': '', 'xuser': cItem['xuser'], 'xpass': cItem['xpass'], 'xhost': cItem['xhost'], 'xua': cItem['xua']})
            self.addDir(params)
            for item in tmp:
                params = dict(cItem)
                params.update({'category': 'serie', 'title': item['category_name'].strip(), 'icon': cItem['icon'], 'category_id': item['category_id'], 'xuser': cItem['xuser'], 'xpass': cItem['xpass'], 'xhost': cItem['xhost'], 'xua': cItem['xua']})
                self.addDir(params)
        except Exception:
            printExc('Cannot parse received data !')

    def ShowMovei(self, cItem):
        printDBG("XtreamVod.ShowMovei cItem[%s]" % (cItem))

        Url = '%s/player_api.php?username=%s&password=%s&action=get_vod_streams&category_id=%s' % (cItem['xhost'], cItem['xuser'], cItem['xpass'], str(cItem['category_id']))

        sts, data = self.getPage(Url)
        if not sts:
            return

        try:
            tmp = json_loads(data)
            for item in tmp:
                tmpUrl = '%s/movie/%s/%s/%s.%s' % (cItem['xhost'], cItem['xuser'], cItem['xpass'], str(item['stream_id']), item['container_extension'])
                if cItem['xua'] != '':
                    tmpUrl = strwithmeta(tmpUrl, {'User-Agent': cItem['xua']})
                stream_icon = item['stream_icon']
                if stream_icon == '':
                    stream_icon = cItem['icon']

                desc = {'rating': item['rating']}
                info = ph.std_title(ensure_str(item['name']), desc=desc, with_ep=True)
                title = info.get('title_display')
                desc = info.get('desc')

                sUrl = '%s/player_api.php?username=%s&password=%s&action=get_vod_info&vod_id=%s' % (cItem['xhost'], cItem['xuser'], cItem['xpass'], str(item['stream_id']))
                params = dict(cItem)
                params.update({
                    'good_for_fav': True, 'EPG': True,  'title': title, 'icon': stream_icon, 'url': tmpUrl, 'url_inf': sUrl, 'desc': desc})
                self.addVideo(params)
        except Exception:
            printExc('Cannot parse received data !')

    def ShowSerie(self, cItem):
        printDBG("XtreamVod.ShowSerie cItem[%s]" % (cItem))

        Url = '%s/player_api.php?username=%s&password=%s&action=get_series&category_id=%s' % (cItem['xhost'], cItem['xuser'], cItem['xpass'], str(cItem['category_id']))

        sts, data = self.getPage(Url)
        if not sts:
            return

        try:
            tmp = json_loads(data)
            for item in tmp:
                stream_icon = item['cover']
                if stream_icon == '':
                    stream_icon = cItem['icon']

                tmpYear = item['releaseDate']
                if tmpYear == '':
                    tmpYear = item['releasedate']

                desc = {'rating': item['rating'], 'genre': item['genre'], 'plot': item['plot'], 'year': self.cm.ph.getSearchGroups(tmpYear, '''(\d{4})''')[0]}
                info = ph.std_title(ensure_str(item['name']), desc=desc, with_ep=True)
                title = info.get('title_display')
                desc = info.get('desc')

                sUrl = '%s/player_api.php?username=%s&password=%s&action=get_series_info&series_id=%s' % (cItem['xhost'], cItem['xuser'], cItem['xpass'], str(item['series_id']))
                params = dict(cItem)
                params.update({
                    'category': 'listSeason', 'good_for_fav': True, 'EPG': True,  'title': title, 'icon': stream_icon, 'url': item['series_id'], 'url_inf': sUrl, 'desc': desc, 'xuser': cItem['xuser'], 'xpass': cItem['xpass'], 'xhost': cItem['xhost'], 'xua': cItem['xua']})
                self.addDir(params)
        except Exception:
            printExc('Cannot parse received data !')

    def exploreItems(self, cItem):
        printDBG("XtreamVod.exploreItems cItem[%s]" % (cItem))

        sUrl = '%s/player_api.php?username=%s&password=%s&action=get_series_info&series_id=%s' % (cItem['xhost'], cItem['xuser'], cItem['xpass'], cItem['url'])
        sts, data = self.getPage(sUrl)
        if not sts:
            return

        try:
            tmp = findall('''['"]episodes['"]:\{(.*)''', data, DOTALL)[0]
            tmp = findall('''['"]id['"]:['"]([^"^']+?)['"].+?['"]title['"]:['"]([^"^']+?)['"].+?['"]container_extension['"]:['"]([^"^']+?)['"]''', tmp, DOTALL)
            for (sID, title, extension) in tmp:
                tmpUrl = '%s/series/%s/%s/%s.%s' % (cItem['xhost'], cItem['xuser'], cItem['xpass'], sID, extension)

                if cItem['xua'] != '':
                    tmpUrl = strwithmeta(tmpUrl, {'User-Agent': cItem['xua']})

                info = ph.std_title(ensure_str(title), with_ep=True)
                title = info.get('title_display')
                otherInfo = '{}\n{}'.format(info.get('desc'), cItem['desc'])

                params = dict(cItem)
                params.update({
                    'good_for_fav': True, 'EPG': True,  'title': title, 'icon': cItem['icon'], 'url': tmpUrl, 'desc': otherInfo})
                self.addVideo(params)
        except Exception:
            printExc('Cannot parse received data !')

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("Aflaam.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        multi_tab = self.getXtreamConf()
        for tmp in multi_tab:
            xhost_ = tmp[1]
            self.addMarker({'title': ' ** '+tmp[0]+' ** ', 'icon': '', 'desc': ''})
            try:
                sUrl = '%s/player_api.php?username=%s&password=%s&action=get_vod_streams' % (xhost_, tmp[2], tmp[3])
                sts, data = self.getPage(sUrl)
                if not sts:
                    return

                data = json_loads(data)
                for item in data:
                    if searchPattern.lower() in item['name'].lower():
                        tmpUrl = '%s/movie/%s/%s/%s.%s' % (xhost_, tmp[2], tmp[3], str(item['stream_id']), item['container_extension'])
                        if tmp[4] != '':
                            tmpUrl = strwithmeta(tmpUrl, {'User-Agent': cItem['xua']})
                        stream_icon = item['stream_icon']
                        if stream_icon == '':
                            stream_icon = cItem['icon']

                        desc = {'rating': item['rating']}
                        info = ph.std_title(ensure_str(item['name']), desc=desc, with_ep=True)
                        title = info.get('title_display')
                        desc = info.get('desc')

                        params = dict(cItem)
                        params.update({
                            'good_for_fav': True, 'EPG': True,  'title': title, 'icon': stream_icon, 'url': tmpUrl, 'desc': desc})
                        self.addVideo(params)
            except:
                pass

            try:
                sUrl = '%s/player_api.php?username=%s&password=%s&action=get_series' % (xhost_, tmp[2], tmp[3])
                sts, data = self.getPage(sUrl)
                if not sts:
                    return

                data = json_loads(data)
                for item in data:
                    if searchPattern.lower() in item['name'].lower():
                        stream_icon = item['cover']
                        if stream_icon == '':
                            stream_icon = cItem['icon']

                        if item['releaseDate'] != '':
                            dt = datetime.strptime(item['releaseDate'], '%Y-%m-%d')
                        elif item['releasedate'] != '':
                            dt = datetime.strptime(item['releasedate'], '%Y-%m-%d')

                        desc = {'rating': item['rating'], 'genre': item['genre'], 'plot': item['plot'], 'year': dt.year}
                        info = ph.std_title(ensure_str(item['name']), desc=desc, with_ep=True)
                        title = info.get('title_display')
                        desc = info.get('desc')

                        sUrl = '%s/player_api.php?username=%s&password=%s&action=get_series_info&series_id=%s' % (cItem['xhost'], cItem['xuser'], cItem['xpass'], str(item['series_id']))
                        params = dict(cItem)
                        params.update({
                            'category': 'listSeason', 'good_for_fav': True, 'EPG': True,  'title': title, 'icon': stream_icon, 'url': item['series_id'], 'url_inf': sUrl, 'desc': desc, 'xuser': cItem['xuser'], 'xpass': cItem['xpass'], 'xhost': cItem['xhost'], 'xua': cItem['xua']})
                        self.addDir(params)
            except:
                pass

    def getLinksForVideo(self, cItem):
        printDBG("Akwam.getLinksForVideo [%s]" % cItem)
        urlTab = []

        urlTab.append({'name': '', 'url': strwithmeta(cItem['url'], {'User-Agent': self.cm.getDefaultUserAgent(browser='mobile')}), 'need_resolve': 0})

        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("XtreamVod.getVideoLinks [%s]" % videoUrl)

        if self.cm.isValidUrl(videoUrl):
            return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG("XtreamVod.getArticleContent [%s]" % cItem)
        otherInfo = {}

        sts, data = self.getPage(cItem['url_inf'])
        if not sts:
            return False

        tmp = json_loads(data)
        tmp = tmp.get('info', [])
        desc = tmp.get('plot', '')
        if tmp.get('age', '') != '':
            otherInfo['age_limit'] = tmp.get('age', '')
        if tmp.get('country', '') != '':
            otherInfo['country'] = tmp.get('country', '')
        if tmp.get('genre', '') != '':
            otherInfo['genre'] = tmp.get('genre', '')
        if tmp.get('duration', '') != '':
            otherInfo['duration'] = tmp.get('duration', '')
        if tmp.get('episode_run_time', '0') != '0':
            otherInfo['duration'] = tmp.get('episode_run_time', '')
        if tmp.get('releasedate', '') != '':
            otherInfo['year'] = tmp.get('releasedate', '')
        if tmp.get('releaseDate', '') != '':
            otherInfo['year'] = tmp.get('releasedate', '')
        if tmp.get('rating', '') != '':
            otherInfo['rating'] = tmp.get('rating', '')

        return [{'title': cItem['title'], 'text': desc, 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

    # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'listItems':
            self.listItems(self.currItem)
        elif category == 'movei':
            self.ShowMovei(self.currItem)
        elif category == 'serie':
            self.ShowSerie(self.currItem)
        elif category == 'listSeason':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, XtreamVod(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
