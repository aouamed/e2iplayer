# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.p2p3.UrlLib import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc


def GetConfigList():
    optionList = []
    return optionList


def gettytul():
    return 'CimaClub'


class CimaClub(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'cimaclub', 'cookie': 'cimaclub.cookie'})

        self.MAIN_URL = 'https://cimaclub.top/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/b3mQWFp/cimaclub.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("CimaClub.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'part0', 'title': _('\c0000??00 الرئيـــســية'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'part1', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'part2', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'part3', 'title': _('أنــمـــي'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG("CimaClub.listCatItems cItem[%s]" % (cItem))
        category = self.currItem.get("category", '')

        if category == 'part0':
            NEW_CAT_TAB = [
                {'category': 'list_items', 'title': _('المضاف حديثاََ'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/getposts?type=one&data=latest')},
                {'category': 'list_items', 'title': _('المثبت'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/getposts?type=one&data=pin')},
                {'category': 'list_items', 'title': _('الاكثر مشاهدة'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/getposts?type=one&data=view')},
                {'category': 'list_items', 'title': _('الأعلي تقييماََ'), 'icon': self.DEFAULT_ICON_URL, 'url': self.getFullUrl('/getposts?type=one&data=rating')}]
            self.listsTab(NEW_CAT_TAB, cItem)
        else:
            if category == 'part1':
                sStart = '> الأفلام'
            elif category == 'part2':
                sStart = '> المسلسلات'
            elif category == 'part3':
                sStart = '>أنمي'
            sts, data = self.getPage(self.getMainUrl())
            if not sts:
                return

            tmp = self.cm.ph.getDataBeetwenMarkers(data, sStart, ('</ul>', '</li>'), True)[1]
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li>', '<a'), ('</a>', '</li>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, '>', '</a>', False)[1])

                params = dict(cItem)
                params.update({'category': 'list_items', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': ''})
                self.addDir(params)

    def listItems(self, cItem):
        printDBG("CimaClub.listItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('pagination', '>'), ('</ul>', '</div>'), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''href=['"]([^"^']+?)['"]>›<''')[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('row MediaGrid', '>'), ('navigation-menu', '>'), True)[1]

        if 'getposts?' in cItem['url']:
            tmp = data

        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('media-block', '>'), ('</div>', '</div>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''data-src=['"]([^"^']+?)['"]''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"] class''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, '<h3>', '</h3>', False)[1])
            rating = ph.extract_desc(item, [('rating', '''ion-ios-star['"]>([^>]+?)[$</]''')])
            desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, '<p>', '</p>', False)[1])

            info = ph.std_title(title, desc=rating, with_ep=True)
            if title != '':
                title = info.get('title_display')
            otherInfo = '{}\n{}'.format(info.get('desc'), desc)

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': otherInfo})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG("CimaClub.exploreItems cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        if 'ajaxCenter' in cItem['url']:
            tmp = self.cm.ph.getAllItemsBeetwenMarkers(data, ('<a', 'Hoverable'), ('</a>', '</li>'))
            for item in tmp:
                url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])

                info = ph.std_title(title.replace('-', ''), with_ep=True)
                if title != '':
                    title = info.get('title_display')
                desc = info.get('desc')

                params = dict(cItem)
                params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': desc})
                self.addVideo(params)

        if '/movie/' in cItem['url']:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': cItem['desc']})
            self.addVideo(params)
        else:
            tmpUrl = self.cm.ph.getDataBeetwenMarkers(data, ('SeriesSection', '>'), ('</div>', '<script>'), True)[1]

            if "Seasons" in tmpUrl:
                self.addMarker({'title': '{}مـــواســم'.format('\c0000??00'), 'icon': cItem['icon'], 'desc': ''})
                tmp = self.cm.ph.getDataBeetwenMarkers(tmpUrl, ('Seasons', '>'), ('</li>', '</ul>'), True)[1]
                tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, '<li', ('</a>', '</li>'))
                for item in tmp:
                    title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])
                    dataSeason = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-season=['"]([^"^']+?)['"]''')[0])
                    dataS = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-S=['"]([^"^']+?)['"]''')[0])
                    dataB = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-B=['"]([^"^']+?)['"]''')[0])

                    info = ph.std_title(title, with_ep=True)
                    if title != '':
                        title = info.get('title_display')
                    desc = info.get('desc')

                    url = self.getFullUrl('ajaxCenter?_action=GetSeasonEp&_season={}&_S={}&_B={}'.format(dataSeason, dataS, dataB))

                    params = dict(cItem)
                    params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': desc})
                    self.addDir(params)

            if "Episodes selected" in tmpUrl:
                self.addMarker({'title': '{}الـحـلـقـات'.format('\c0000??00'), 'icon': cItem['icon'], 'desc': ''})
                tmp = self.cm.ph.getDataBeetwenMarkers(tmpUrl, ('Episodes selected', '>'), ('</ul>', '</div>'), True)[1]
                tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<a', 'Hoverable'), ('</a>', '</li>'))
                for item in tmp:
                    url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                    title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])

                    info = ph.std_title(title.replace('-', ''), with_ep=True)
                    if title != '':
                        title = info.get('title_display')
                    desc = info.get('desc')

                    params = dict(cItem)
                    params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': desc})
                    self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("CimaClub.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        url = self.getFullUrl('/search?s={}'.format(urllib_quote_plus(searchPattern)))
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG("CimaClub.getLinksForVideo [%s]" % cItem)
        urlTab = []

        baseUrl = cItem['url'].replace('movie', 'watch').replace('episode', 'watch').replace('anime', 'watch')
        sts, data = self.getPage(baseUrl)
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('servers-tabs', '>'), ('player-wraper', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, '<li', '</li>')
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''data-embedd=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, '<em>', '</em>', False)[1])

            if title != '':
                title = ('{} \c00??7950 [{}]\c00??????\c00???020 - {}\c00??????'.format(cItem['title'], title, self.up.getHostName(url, True)))
            urlTab.append({'name': title, 'url': url, 'need_resolve': 1})
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("CimaClub.getVideoLinks [%s]" % videoUrl)

        return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG("CimaClub.getArticleContent [%s]" % cItem)
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('media-details', '>'), 'DOMContentLoaded', True)[1]

        title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, '<h1>', '</h1>', False)[1])
        if title == '':
            title = cItem['title']

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('<p', 'Scroll-list', '>'), ('</p>', '<div'), False)[1])
        if desc == '':
            desc = cItem['desc']

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('ion-ios-star-outline', '>'), '</span>', False)[1])
        if Info != '':
            otherInfo['imdb_rating'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('ion-md-time', '<span>'), '</span>', False)[1])
        if Info != '':
            otherInfo['duration'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('ion-md-calendar', '<span>'), '</span>', False)[1])
        if Info != '':
            otherInfo['year'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('ion-ios-tv', '<span>'), '</span>', False)[1])
        if Info != '':
            otherInfo['quality'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('genre', '>'), '</li>', False)[1])
        if Info != '':
            otherInfo['genre'] = Info

        return [{'title': title, 'text': desc, 'images': [{'title': '', 'url': cItem['icon']}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

        # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'part0' or category == 'part1' or category == 'part2' or category == 'part3':
            self.listCatItems(self.currItem)
        elif category == 'list_items':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, CimaClub(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
