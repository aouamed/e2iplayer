# -*- coding: utf-8 -*-
# Coding: BY MOHAMED_OS


from Plugins.Extensions.IPTVPlayer.components.ihost import (CBaseHostClass,
                                                            CHostBase)
from Plugins.Extensions.IPTVPlayer.components.iptvplayerinit import \
    TranslateTXT as _
from Plugins.Extensions.IPTVPlayer.libs import ph
from Plugins.Extensions.IPTVPlayer.p2p3.UrlLib import urllib_quote_plus
from Plugins.Extensions.IPTVPlayer.tools.iptvtools import printDBG, printExc


def GetConfigList():
    optionList = []
    return optionList

def gettytul():
    return 'Extra 3sk'


class Extra3sk(CBaseHostClass):

    def __init__(self):
        CBaseHostClass.__init__(self, {'history': 'extrask', 'cookie': 'extrask.cookie'})

        self.MAIN_URL = 'https://w2.extrask.art/'
        self.DEFAULT_ICON_URL = 'https://i.ibb.co/WVmn0BT/Extra-3sk.png'

        self.HEADER = self.cm.getDefaultHeader()
        self.AJAX_HEADER = self.HEADER
        self.AJAX_HEADER.update({'X-Requested-With': 'XMLHttpRequest'})
        self.defaultParams = {'header': self.HEADER, 'use_cookie': True, 'load_cookie': True, 'save_cookie': True, 'cookiefile': self.COOKIE_FILE}

    def getPage(self, baseUrl, addParams={}, post_data=None):
        if addParams == {}:
            addParams = dict(self.defaultParams)

        sts, data = self.cm.getPage(self.cm.ph.stdUrl(baseUrl), addParams, post_data)
        return sts, data

    def listMainMenu(self, cItem):
        printDBG("Extra 3sk.listMainMenu")
        MAIN_CAT_TAB = [
            {'category': 'movei', 'title': _('الأفـــلام'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'serie', 'title': _('مســلـســلات'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'asia', 'title': _('مســلـســلات آسـيويـة'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'other', 'title': _('رمـــضــان'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'tvshow', 'title': _('بـــرامج'), 'icon': self.DEFAULT_ICON_URL},
            {'category': 'search', 'title': _('Search'), 'search_item': True},
            {'category': 'search_history', 'title': _('Search history'), }]
        self.listsTab(MAIN_CAT_TAB, cItem)

    def listCatItems(self, cItem):
        printDBG("Extra 3sk.listCatItems cItem[%s]" % (cItem))
        category = self.currItem.get("category", '')

        sts, data = self.getPage(self.getMainUrl())
        if not sts:
            return

        if category == 'movei':
            sStart = '>الأفلام'
        elif category == 'serie':
            sStart = '>مسلسلات'
        elif category == 'asia':
            sStart = '>مسلسلات آسيوية'
        elif category == 'tvshow':
            sStart = '>برامج'
        elif category == 'other':
            sStart = '>رمضان'

        tmp = self.cm.ph.getDataBeetwenMarkers(data, sStart, ('</ul>', '</li>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', 'menu-item'), ('</li', '>'))
        for item in tmp:
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])

            params = dict(cItem)
            params.update({'category': 'list_items', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': ''})
            self.addDir(params)

    def listItems(self, cItem):
        printDBG("Extra 3sk.listItems cItem[%s]" % (cItem))
        page = cItem.get('page', 1)

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        nextPage = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'navigation'), ('</div', '>',), True)[1]
        nextPage = self.getFullUrl(self.cm.ph.getSearchGroups(nextPage, '''href=['"]([^'^"]+?)['"][^>]*?>%s<''' % (page + 1))[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'containers container-fluid'), ('<div', '>', 'col-md-12'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<article', '>', 'post'), ('</article', '>'))
        for item in tmp:
            icon = self.getFullIconUrl(self.cm.ph.getSearchGroups(item, '''url\((.+?)\)''')[0])
            url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''title=['"]([^"^']+?)['"]''')[0])
            desc = ph.extract_desc(item, [('rating', '''icon-star-full2 f13['"].+?([^>]+?)[$<]''')])

            info = ph.std_title(title, desc=desc, with_ep=True)
            if title != '':
                title = info.get('title_display')
            desc = info.get('desc')

            params = dict(cItem)
            params.update({'category': 'listItems', 'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': self.cm.ph.stdUrl(icon), 'desc': desc})
            self.addDir(params)

        if nextPage != '':
            params = dict(cItem)
            params.update({'title': _("Next page"), 'url': nextPage, 'page': page + 1})
            self.addDir(params)

    def exploreItems(self, cItem):
        printDBG("Extra 3sk.exploreItems cItem[%s]" % (cItem))

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        desc = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(data, ('<h3', '>', 'story'), ('</h3', '>'), False)[1]).replace('القصه', '')

        if 'حلقات المسلسل' in data:
            Season = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'listSeasons'), ('</ul', '>'), True)[1]
            if Season:
                self.addMarker({'title': '{}مـــواســم'.format('\c0000??00'), 'icon': cItem['icon'], 'desc': ''})
                tmp = self.cm.ph.getAllItemsBeetwenMarkers(Season, '<li', ('</li', '>'))
                for item in tmp:
                    tmpUrl = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''data-season=['"]([^"^']+?)['"]''')[0])
                    title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('data-season', '>'), ('</li', '>'), False)[1])

                    info = ph.std_title(title, with_ep=True)
                    if title != '':
                        title = info.get('title_display')
                    otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                    if tmpUrl != '':
                        url = self.getFullUrl('?p=%s' % tmpUrl)

                    params = dict(cItem)
                    params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                    self.addDir(params)

            Episod = self.cm.ph.getDataBeetwenMarkers(data, ('<ul', '>', 'eplist'), ('</ul', '>'), True)[1]
            if Episod:
                self.addMarker({'title': '{}الـحـلـقـات'.format('\c0000??00'), 'icon': cItem['icon'], 'desc': ''})
                tmp = self.cm.ph.getAllItemsBeetwenMarkers(Episod, ('<a', '>', 'epNum'), ('</a', '>'))
                for item in tmp:
                    url = self.getFullUrl(self.cm.ph.getSearchGroups(item, '''href=['"]([^"^']+?)['"]''')[0])
                    title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('<em', '>'), ('</span', '>'), False)[1])

                    info = ph.std_title(title, with_ep=True)
                    if title != '':
                        title = info.get('title_display')
                    otherInfo = '{}\n{}'.format(info.get('desc'), desc)

                    params = dict(cItem)
                    params.update({'good_for_fav': True, 'EPG': True, 'title': title, 'url': url, 'icon': cItem['icon'], 'desc': otherInfo})
                    self.addVideo(params)
        else:
            params = dict(cItem)
            params.update({'good_for_fav': True, 'EPG': True, 'title': cItem['title'], 'url': cItem['url'], 'icon': cItem['icon'], 'desc': desc})
            self.addVideo(params)

    def listSearchResult(self, cItem, searchPattern, searchType):
        printDBG("Extra 3sk.listSearchResult cItem[%s], searchPattern[%s] searchType[%s]" % (cItem, searchPattern, searchType))
        url = self.getFullUrl('/?s={}'.format(urllib_quote_plus(searchPattern)))
        params = {'name': 'category', 'category': 'list_items', 'good_for_fav': False, 'url': url}
        self.listItems(params)

    def getLinksForVideo(self, cItem):
        printDBG("Extra 3sk.getLinksForVideo [%s]" % cItem)
        urlTab = []
        baseUrl = '%s?do=views' % cItem['url']

        sts, data = self.getPage(baseUrl)
        if not sts:
            return

        postID = self.cleanHtmlStr(self.cm.ph.getSearchGroups(data, '''vo_postID = ['"]([^"^']+?)['"]''')[0])

        tmp = self.cm.ph.getDataBeetwenMarkers(data, '>سيرفرات المشاهدة', ('</ul', '>'), True)[1]
        tmp = self.cm.ph.getAllItemsBeetwenMarkers(tmp, ('<li', '>'), ('</li', '>'))
        for item in tmp:
            tmpID = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''this.id.+?([^,]+?)[$,]''')[0])
            tmpServ = self.cleanHtmlStr(self.cm.ph.getSearchGroups(item, '''this.id,.+?,([^,]+?)[$)]''')[0])
            title = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(item, ('</i', '>'), ('</a', '>'), False)[1])

            tmpUrl = self.getFullUrl('wp-content/themes/vo2020/temp/ajax/iframe2.php?id=%s&video=%s&serverId=%s' % (postID, tmpID, tmpServ))
            sts, data = self.getPage(tmpUrl)
            if not sts:
                return

            url = self.getFullUrl(self.cm.ph.getSearchGroups(data, '''SRC=['"]([^"^']+?)['"]''', ignoreCase=True)[0])

            if title != '':
                title = ('{} \c00??7950 [{}]\c00??????\c00???020 - {}\c00??????'.format(cItem['title'], title, self.up.getHostName(url, True)))

            urlTab.append({'name': title, 'url': url, 'need_resolve': 1})
        return urlTab

    def getVideoLinks(self, videoUrl):
        printDBG("Extra 3sk.getVideoLinks [%s]" % videoUrl)

        if self.cm.isValidUrl(videoUrl):
            return self.up.getVideoLinkExt(videoUrl)

    def getArticleContent(self, cItem):
        printDBG("Extra 3sk.getArticleContent [%s]" % cItem)
        otherInfo = {}

        sts, data = self.getPage(cItem['url'])
        if not sts:
            return

        tmp = self.cm.ph.getDataBeetwenMarkers(data, ('<div', '>', 'single-info-poster'), ('<div', '>', 'single-info-foot'), True)[1]

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('count-views', '</i>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['views'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('icon-star-full2 f12', 'IMDb'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['imdb_rating'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('icon-alarm', '<a>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['duration'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('quality', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['quality'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('year', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['year'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('genre', '>'), ('</ul', '>'), False)[1])
        if Info != '':
            otherInfo['genre'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('country', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['country'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('icon-earth f12', '<a>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['language'] = Info

        Info = self.cleanHtmlStr(self.cm.ph.getDataBeetwenNodes(tmp, ('category', '>'), ('</a', '>'), False)[1])
        if Info != '':
            otherInfo['category'] = Info

        return [{'title': cItem['title'], 'text': cItem['desc'], 'images': [{'title': '', 'url': self.getFullUrl(cItem['icon'])}], 'other_info': otherInfo}]

    def handleService(self, index, refresh=0, searchPattern='', searchType=''):
        printDBG('handleService start')

        CBaseHostClass.handleService(self, index, refresh, searchPattern, searchType)

        name = self.currItem.get("name", '')
        category = self.currItem.get("category", '')
        printDBG("handleService: |||||||||||||||||||||||||||||||||||| name[%s], category[%s] " % (name, category))
        self.currList = []

    # MAIN MENU
        if name is None and category == '':
            self.listMainMenu({'name': 'category', 'type': 'category'})
        elif category == 'movei' or category == 'serie' or category == 'other' or category == 'asia' or category == 'tvshow':
            self.listCatItems(self.currItem)
        elif category == 'list_items' or category == 'home':
            self.listItems(self.currItem)
        elif category == 'listItems':
            self.exploreItems(self.currItem)
    # SEARCH
        elif category in ["search", "search_next_page"]:
            cItem = dict(self.currItem)
            cItem.update({'search_item': False, 'name': 'category'})
            self.listSearchResult(cItem, searchPattern, searchType)
    # HISTORIA SEARCH
        elif category == "search_history":
            self.listsHistory({'name': 'history', 'category': 'search'}, 'desc', _("Type: "))
        else:
            printExc()

        CBaseHostClass.endHandleService(self, index, refresh)


class IPTVHost(CHostBase):

    def __init__(self):
        CHostBase.__init__(self, Extra3sk(), True, [])

    def withArticleContent(self, cItem):
        if cItem['type'] != 'video' and cItem['category'] != 'exploreItems':
            return False
        return True
